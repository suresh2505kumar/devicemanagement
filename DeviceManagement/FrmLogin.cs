﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Configuration;
using System.Collections.Generic;
using System.Management;

namespace DeviceManagement
{
    public partial class FrmLogin : Form
    {
        public string name;
        public FrmLogin()
        {
            InitializeComponent();
        }
      
        private void BtnLogin_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
            conn.Open();
            //string Query = "Select * from Useradmin where NAME='" + txtUserName.Text + "' and ADMINPWD='" + TxtPassword.Text + "'";
            string Query = "Select * from MyUseradmin where UserName='" + txtUserName.Text + "' and Password='" + TxtPassword.Text + "'";
            SqlCommand cmd = new SqlCommand(Query,conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conn.Close();
            cmd.Dispose();            
            if(dt.Rows.Count==1)
            {
                this.Hide();
                GlobalVar.Branch = txtBranch.Text;
                //GlobalVar.sUserId = Convert.ToInt32(dt.Rows[0]["UserID"].ToString());
                GlobalVar.sUserId = Convert.ToInt32(dt.Rows[0]["Uid"].ToString());
                MDImain main = new MDImain();
                main.Show();
            }           
            else
            {
                MessageBox.Show("Worng Username Password");
                return;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            List<string> results = new List<string>();
            results =  GetCpuIds();
            foreach (var item in results)
            {
                lblLicense.Text = item.ToString();
            }
            string branch = ConfigurationManager.AppSettings["Branch"].ToString();
            txtBranch.Text = branch;
            GlobalVar.Branch = branch;
        }
        private List<string> GetCpuIds()
        {
            List<string> results = new List<string>();

            string query = "Select * FROM Win32_Processor";
            ManagementObjectSearcher searcher =
                new ManagementObjectSearcher(query);
            foreach (ManagementObject info in searcher.Get())
            {
                results.Add(
                    info.GetPropertyValue("ProcessorId").ToString());
            }
            return results;
        }
    }
}
