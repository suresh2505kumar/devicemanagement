﻿namespace DeviceManagement
{
    partial class FrmUserInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grFront = new System.Windows.Forms.GroupBox();
            this.tabl1 = new System.Windows.Forms.TabControl();
            this.UserInfo = new System.Windows.Forms.TabPage();
            this.grSeletecUsers = new System.Windows.Forms.GroupBox();
            this.chckFromDB = new System.Windows.Forms.CheckBox();
            this.txtEmpCodeSearch = new System.Windows.Forms.TextBox();
            this.btnFacebackward = new System.Windows.Forms.Button();
            this.btnFaceForward = new System.Windows.Forms.Button();
            this.btnFaceUploadSelectedUsers = new System.Windows.Forms.Button();
            this.DataGridFaceTo = new System.Windows.Forms.DataGridView();
            this.DataGridFaceFrom = new System.Windows.Forms.DataGridView();
            this.FPUserInfo = new System.Windows.Forms.TabPage();
            this.chckFeSelectedUsers = new System.Windows.Forms.CheckBox();
            this.grFpBulk = new System.Windows.Forms.GroupBox();
            this.btnFpBulkUpload = new System.Windows.Forms.Button();
            this.DataGridFpBulk = new System.Windows.Forms.DataGridView();
            this.grFpSelectedUsers = new System.Windows.Forms.GroupBox();
            this.txtFpSearch = new System.Windows.Forms.TextBox();
            this.btnFpBackward = new System.Windows.Forms.Button();
            this.btnFpForward = new System.Windows.Forms.Button();
            this.btnFpSelectedUsers = new System.Windows.Forms.Button();
            this.DataGridFpTo = new System.Windows.Forms.DataGridView();
            this.DataGridFpFrom = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.chckFace = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnSource = new System.Windows.Forms.Button();
            this.btnConnectSourceIp = new System.Windows.Forms.Button();
            this.cmbTargetIp = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbip = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDownloadUser = new System.Windows.Forms.Button();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.lblstate = new System.Windows.Forms.Label();
            this.grFront.SuspendLayout();
            this.tabl1.SuspendLayout();
            this.UserInfo.SuspendLayout();
            this.grSeletecUsers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFaceTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFaceFrom)).BeginInit();
            this.FPUserInfo.SuspendLayout();
            this.grFpBulk.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFpBulk)).BeginInit();
            this.grFpSelectedUsers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFpTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFpFrom)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.tabl1);
            this.grFront.Controls.Add(this.groupBox1);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(8, 2);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(707, 555);
            this.grFront.TabIndex = 0;
            this.grFront.TabStop = false;
            // 
            // tabl1
            // 
            this.tabl1.Controls.Add(this.UserInfo);
            this.tabl1.Controls.Add(this.FPUserInfo);
            this.tabl1.Location = new System.Drawing.Point(4, 94);
            this.tabl1.Name = "tabl1";
            this.tabl1.SelectedIndex = 0;
            this.tabl1.Size = new System.Drawing.Size(697, 455);
            this.tabl1.TabIndex = 1;
            // 
            // UserInfo
            // 
            this.UserInfo.Controls.Add(this.grSeletecUsers);
            this.UserInfo.Location = new System.Drawing.Point(4, 27);
            this.UserInfo.Name = "UserInfo";
            this.UserInfo.Padding = new System.Windows.Forms.Padding(3);
            this.UserInfo.Size = new System.Drawing.Size(689, 424);
            this.UserInfo.TabIndex = 0;
            this.UserInfo.Text = "UserInfo";
            this.UserInfo.UseVisualStyleBackColor = true;
            // 
            // grSeletecUsers
            // 
            this.grSeletecUsers.Controls.Add(this.chckFromDB);
            this.grSeletecUsers.Controls.Add(this.txtEmpCodeSearch);
            this.grSeletecUsers.Controls.Add(this.btnFacebackward);
            this.grSeletecUsers.Controls.Add(this.btnFaceForward);
            this.grSeletecUsers.Controls.Add(this.btnFaceUploadSelectedUsers);
            this.grSeletecUsers.Controls.Add(this.DataGridFaceTo);
            this.grSeletecUsers.Controls.Add(this.DataGridFaceFrom);
            this.grSeletecUsers.Location = new System.Drawing.Point(3, 6);
            this.grSeletecUsers.Name = "grSeletecUsers";
            this.grSeletecUsers.Size = new System.Drawing.Size(680, 408);
            this.grSeletecUsers.TabIndex = 8;
            this.grSeletecUsers.TabStop = false;
            // 
            // chckFromDB
            // 
            this.chckFromDB.AutoSize = true;
            this.chckFromDB.Location = new System.Drawing.Point(11, 11);
            this.chckFromDB.Name = "chckFromDB";
            this.chckFromDB.Size = new System.Drawing.Size(141, 22);
            this.chckFromDB.TabIndex = 2;
            this.chckFromDB.Text = "Get Users From DB";
            this.chckFromDB.UseVisualStyleBackColor = true;
            this.chckFromDB.CheckedChanged += new System.EventHandler(this.chckSelectedUsers_CheckedChanged);
            // 
            // txtEmpCodeSearch
            // 
            this.txtEmpCodeSearch.Location = new System.Drawing.Point(11, 39);
            this.txtEmpCodeSearch.Name = "txtEmpCodeSearch";
            this.txtEmpCodeSearch.Size = new System.Drawing.Size(318, 26);
            this.txtEmpCodeSearch.TabIndex = 9;
            this.txtEmpCodeSearch.TextChanged += new System.EventHandler(this.txtEmpCodeSearch_TextChanged);
            // 
            // btnFacebackward
            // 
            this.btnFacebackward.Location = new System.Drawing.Point(332, 178);
            this.btnFacebackward.Name = "btnFacebackward";
            this.btnFacebackward.Size = new System.Drawing.Size(32, 26);
            this.btnFacebackward.TabIndex = 8;
            this.btnFacebackward.Text = "<";
            this.btnFacebackward.UseVisualStyleBackColor = true;
            // 
            // btnFaceForward
            // 
            this.btnFaceForward.Location = new System.Drawing.Point(332, 131);
            this.btnFaceForward.Name = "btnFaceForward";
            this.btnFaceForward.Size = new System.Drawing.Size(32, 26);
            this.btnFaceForward.TabIndex = 7;
            this.btnFaceForward.Text = ">";
            this.btnFaceForward.UseVisualStyleBackColor = true;
            this.btnFaceForward.Click += new System.EventHandler(this.btnFaceForward_Click);
            // 
            // btnFaceUploadSelectedUsers
            // 
            this.btnFaceUploadSelectedUsers.Location = new System.Drawing.Point(563, 35);
            this.btnFaceUploadSelectedUsers.Name = "btnFaceUploadSelectedUsers";
            this.btnFaceUploadSelectedUsers.Size = new System.Drawing.Size(111, 28);
            this.btnFaceUploadSelectedUsers.TabIndex = 2;
            this.btnFaceUploadSelectedUsers.Text = "Upload Data";
            this.btnFaceUploadSelectedUsers.UseVisualStyleBackColor = true;
            this.btnFaceUploadSelectedUsers.Click += new System.EventHandler(this.btnFaceUploadSelectedUsers_Click);
            // 
            // DataGridFaceTo
            // 
            this.DataGridFaceTo.BackgroundColor = System.Drawing.Color.White;
            this.DataGridFaceTo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridFaceTo.Location = new System.Drawing.Point(366, 63);
            this.DataGridFaceTo.Name = "DataGridFaceTo";
            this.DataGridFaceTo.RowHeadersVisible = false;
            this.DataGridFaceTo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridFaceTo.Size = new System.Drawing.Size(308, 339);
            this.DataGridFaceTo.TabIndex = 1;
            // 
            // DataGridFaceFrom
            // 
            this.DataGridFaceFrom.BackgroundColor = System.Drawing.Color.White;
            this.DataGridFaceFrom.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridFaceFrom.Location = new System.Drawing.Point(11, 67);
            this.DataGridFaceFrom.Name = "DataGridFaceFrom";
            this.DataGridFaceFrom.RowHeadersVisible = false;
            this.DataGridFaceFrom.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridFaceFrom.Size = new System.Drawing.Size(318, 335);
            this.DataGridFaceFrom.TabIndex = 0;
            // 
            // FPUserInfo
            // 
            this.FPUserInfo.Controls.Add(this.chckFeSelectedUsers);
            this.FPUserInfo.Controls.Add(this.grFpBulk);
            this.FPUserInfo.Controls.Add(this.grFpSelectedUsers);
            this.FPUserInfo.Location = new System.Drawing.Point(4, 27);
            this.FPUserInfo.Name = "FPUserInfo";
            this.FPUserInfo.Padding = new System.Windows.Forms.Padding(3);
            this.FPUserInfo.Size = new System.Drawing.Size(689, 424);
            this.FPUserInfo.TabIndex = 1;
            this.FPUserInfo.Text = "FP UserInfo";
            this.FPUserInfo.UseVisualStyleBackColor = true;
            // 
            // chckFeSelectedUsers
            // 
            this.chckFeSelectedUsers.AutoSize = true;
            this.chckFeSelectedUsers.Location = new System.Drawing.Point(565, 5);
            this.chckFeSelectedUsers.Name = "chckFeSelectedUsers";
            this.chckFeSelectedUsers.Size = new System.Drawing.Size(118, 22);
            this.chckFeSelectedUsers.TabIndex = 11;
            this.chckFeSelectedUsers.Text = "Selected Users";
            this.chckFeSelectedUsers.UseVisualStyleBackColor = true;
            this.chckFeSelectedUsers.CheckedChanged += new System.EventHandler(this.chckFeSelectedUsers_CheckedChanged);
            // 
            // grFpBulk
            // 
            this.grFpBulk.Controls.Add(this.btnFpBulkUpload);
            this.grFpBulk.Controls.Add(this.DataGridFpBulk);
            this.grFpBulk.Location = new System.Drawing.Point(6, 23);
            this.grFpBulk.Name = "grFpBulk";
            this.grFpBulk.Size = new System.Drawing.Size(679, 392);
            this.grFpBulk.TabIndex = 9;
            this.grFpBulk.TabStop = false;
            // 
            // btnFpBulkUpload
            // 
            this.btnFpBulkUpload.Location = new System.Drawing.Point(546, 355);
            this.btnFpBulkUpload.Name = "btnFpBulkUpload";
            this.btnFpBulkUpload.Size = new System.Drawing.Size(124, 31);
            this.btnFpBulkUpload.TabIndex = 7;
            this.btnFpBulkUpload.Text = "Upload";
            this.btnFpBulkUpload.UseVisualStyleBackColor = true;
            // 
            // DataGridFpBulk
            // 
            this.DataGridFpBulk.AllowUserToAddRows = false;
            this.DataGridFpBulk.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridFpBulk.Location = new System.Drawing.Point(4, 14);
            this.DataGridFpBulk.Name = "DataGridFpBulk";
            this.DataGridFpBulk.ReadOnly = true;
            this.DataGridFpBulk.RowHeadersVisible = false;
            this.DataGridFpBulk.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridFpBulk.Size = new System.Drawing.Size(667, 339);
            this.DataGridFpBulk.TabIndex = 0;
            // 
            // grFpSelectedUsers
            // 
            this.grFpSelectedUsers.Controls.Add(this.txtFpSearch);
            this.grFpSelectedUsers.Controls.Add(this.btnFpBackward);
            this.grFpSelectedUsers.Controls.Add(this.btnFpForward);
            this.grFpSelectedUsers.Controls.Add(this.btnFpSelectedUsers);
            this.grFpSelectedUsers.Controls.Add(this.DataGridFpTo);
            this.grFpSelectedUsers.Controls.Add(this.DataGridFpFrom);
            this.grFpSelectedUsers.Location = new System.Drawing.Point(4, 23);
            this.grFpSelectedUsers.Name = "grFpSelectedUsers";
            this.grFpSelectedUsers.Size = new System.Drawing.Size(680, 390);
            this.grFpSelectedUsers.TabIndex = 10;
            this.grFpSelectedUsers.TabStop = false;
            // 
            // txtFpSearch
            // 
            this.txtFpSearch.Location = new System.Drawing.Point(11, 14);
            this.txtFpSearch.Name = "txtFpSearch";
            this.txtFpSearch.Size = new System.Drawing.Size(318, 26);
            this.txtFpSearch.TabIndex = 9;
            // 
            // btnFpBackward
            // 
            this.btnFpBackward.Location = new System.Drawing.Point(332, 153);
            this.btnFpBackward.Name = "btnFpBackward";
            this.btnFpBackward.Size = new System.Drawing.Size(32, 26);
            this.btnFpBackward.TabIndex = 8;
            this.btnFpBackward.Text = "<";
            this.btnFpBackward.UseVisualStyleBackColor = true;
            // 
            // btnFpForward
            // 
            this.btnFpForward.Location = new System.Drawing.Point(332, 106);
            this.btnFpForward.Name = "btnFpForward";
            this.btnFpForward.Size = new System.Drawing.Size(32, 26);
            this.btnFpForward.TabIndex = 7;
            this.btnFpForward.Text = ">";
            this.btnFpForward.UseVisualStyleBackColor = true;
            // 
            // btnFpSelectedUsers
            // 
            this.btnFpSelectedUsers.Location = new System.Drawing.Point(563, 10);
            this.btnFpSelectedUsers.Name = "btnFpSelectedUsers";
            this.btnFpSelectedUsers.Size = new System.Drawing.Size(111, 28);
            this.btnFpSelectedUsers.TabIndex = 2;
            this.btnFpSelectedUsers.Text = "Upload";
            this.btnFpSelectedUsers.UseVisualStyleBackColor = true;
            // 
            // DataGridFpTo
            // 
            this.DataGridFpTo.AllowUserToAddRows = false;
            this.DataGridFpTo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridFpTo.Location = new System.Drawing.Point(366, 38);
            this.DataGridFpTo.Name = "DataGridFpTo";
            this.DataGridFpTo.ReadOnly = true;
            this.DataGridFpTo.RowHeadersVisible = false;
            this.DataGridFpTo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridFpTo.Size = new System.Drawing.Size(308, 339);
            this.DataGridFpTo.TabIndex = 1;
            // 
            // DataGridFpFrom
            // 
            this.DataGridFpFrom.AllowUserToAddRows = false;
            this.DataGridFpFrom.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridFpFrom.Location = new System.Drawing.Point(11, 42);
            this.DataGridFpFrom.Name = "DataGridFpFrom";
            this.DataGridFpFrom.ReadOnly = true;
            this.DataGridFpFrom.RowHeadersVisible = false;
            this.DataGridFpFrom.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridFpFrom.Size = new System.Drawing.Size(318, 335);
            this.DataGridFpFrom.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.chckFace);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.btnSource);
            this.groupBox1.Controls.Add(this.btnConnectSourceIp);
            this.groupBox1.Controls.Add(this.cmbTargetIp);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cmbip);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnDownloadUser);
            this.groupBox1.Controls.Add(this.txtPort);
            this.groupBox1.Controls.Add(this.lblstate);
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(707, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(603, 72);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(66, 22);
            this.checkBox1.TabIndex = 11;
            this.checkBox1.Text = "Finger";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // chckFace
            // 
            this.chckFace.AutoSize = true;
            this.chckFace.Location = new System.Drawing.Point(538, 71);
            this.chckFace.Name = "chckFace";
            this.chckFace.Size = new System.Drawing.Size(55, 22);
            this.chckFace.TabIndex = 10;
            this.chckFace.Text = "Face";
            this.chckFace.UseVisualStyleBackColor = true;
            this.chckFace.Visible = false;
            this.chckFace.CheckedChanged += new System.EventHandler(this.chckFace_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(241, 62);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(182, 31);
            this.button1.TabIndex = 9;
            this.button1.Text = "Connect Target";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSource
            // 
            this.btnSource.Location = new System.Drawing.Point(203, 34);
            this.btnSource.Name = "btnSource";
            this.btnSource.Size = new System.Drawing.Size(32, 26);
            this.btnSource.TabIndex = 5;
            this.btnSource.Text = ">";
            this.btnSource.UseVisualStyleBackColor = true;
            this.btnSource.Click += new System.EventHandler(this.BtnSource_Click);
            // 
            // btnConnectSourceIp
            // 
            this.btnConnectSourceIp.Location = new System.Drawing.Point(15, 62);
            this.btnConnectSourceIp.Name = "btnConnectSourceIp";
            this.btnConnectSourceIp.Size = new System.Drawing.Size(182, 31);
            this.btnConnectSourceIp.TabIndex = 4;
            this.btnConnectSourceIp.Text = "Connect Source";
            this.btnConnectSourceIp.UseVisualStyleBackColor = true;
            this.btnConnectSourceIp.Click += new System.EventHandler(this.BtnConnectSourceIp_Click);
            // 
            // cmbTargetIp
            // 
            this.cmbTargetIp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTargetIp.FormattingEnabled = true;
            this.cmbTargetIp.Location = new System.Drawing.Point(241, 34);
            this.cmbTargetIp.Name = "cmbTargetIp";
            this.cmbTargetIp.Size = new System.Drawing.Size(182, 26);
            this.cmbTargetIp.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(242, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Target Machine";
            // 
            // cmbip
            // 
            this.cmbip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbip.FormattingEnabled = true;
            this.cmbip.Location = new System.Drawing.Point(15, 34);
            this.cmbip.Name = "cmbip";
            this.cmbip.Size = new System.Drawing.Size(182, 26);
            this.cmbip.TabIndex = 1;
            this.cmbip.SelectedIndexChanged += new System.EventHandler(this.cmbip_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Source Machine";
            // 
            // btnDownloadUser
            // 
            this.btnDownloadUser.Location = new System.Drawing.Point(538, 32);
            this.btnDownloadUser.Name = "btnDownloadUser";
            this.btnDownloadUser.Size = new System.Drawing.Size(124, 31);
            this.btnDownloadUser.TabIndex = 6;
            this.btnDownloadUser.Text = "Download";
            this.btnDownloadUser.UseVisualStyleBackColor = true;
            this.btnDownloadUser.Click += new System.EventHandler(this.BtnDownloadUser_Click);
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(661, 34);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(40, 26);
            this.txtPort.TabIndex = 7;
            this.txtPort.Text = "4370";
            this.txtPort.Visible = false;
            // 
            // lblstate
            // 
            this.lblstate.Location = new System.Drawing.Point(364, 13);
            this.lblstate.Name = "lblstate";
            this.lblstate.Size = new System.Drawing.Size(306, 19);
            this.lblstate.TabIndex = 8;
            this.lblstate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FrmUserInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(727, 569);
            this.Controls.Add(this.grFront);
            this.MaximizeBox = false;
            this.Name = "FrmUserInfo";
            this.Text = "Source Machine";
            this.Load += new System.EventHandler(this.FrmUserInfo_Load);
            this.grFront.ResumeLayout(false);
            this.tabl1.ResumeLayout(false);
            this.UserInfo.ResumeLayout(false);
            this.grSeletecUsers.ResumeLayout(false);
            this.grSeletecUsers.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFaceTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFaceFrom)).EndInit();
            this.FPUserInfo.ResumeLayout(false);
            this.FPUserInfo.PerformLayout();
            this.grFpBulk.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFpBulk)).EndInit();
            this.grFpSelectedUsers.ResumeLayout(false);
            this.grFpSelectedUsers.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFpTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFpFrom)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbTargetIp;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbip;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSource;
        private System.Windows.Forms.Button btnConnectSourceIp;
        private System.Windows.Forms.Button btnDownloadUser;
        private System.Windows.Forms.TabControl tabl1;
        private System.Windows.Forms.TabPage UserInfo;
        private System.Windows.Forms.TabPage FPUserInfo;
        private System.Windows.Forms.CheckBox chckFromDB;
        private System.Windows.Forms.GroupBox grSeletecUsers;
        private System.Windows.Forms.Button btnFacebackward;
        private System.Windows.Forms.Button btnFaceForward;
        private System.Windows.Forms.Button btnFaceUploadSelectedUsers;
        private System.Windows.Forms.DataGridView DataGridFaceTo;
        private System.Windows.Forms.DataGridView DataGridFaceFrom;
        private System.Windows.Forms.TextBox txtEmpCodeSearch;
        private System.Windows.Forms.CheckBox chckFeSelectedUsers;
        private System.Windows.Forms.GroupBox grFpBulk;
        private System.Windows.Forms.Button btnFpBulkUpload;
        private System.Windows.Forms.DataGridView DataGridFpBulk;
        private System.Windows.Forms.GroupBox grFpSelectedUsers;
        private System.Windows.Forms.TextBox txtFpSearch;
        private System.Windows.Forms.Button btnFpBackward;
        private System.Windows.Forms.Button btnFpForward;
        private System.Windows.Forms.Button btnFpSelectedUsers;
        private System.Windows.Forms.DataGridView DataGridFpTo;
        private System.Windows.Forms.DataGridView DataGridFpFrom;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Label lblstate;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox chckFace;
    }
}