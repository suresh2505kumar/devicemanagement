﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeviceManagement
{
    public partial class MDImain : Form
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        public MDImain()
        {
            InitializeComponent();
            this.FormClosing += Form1_FormClosing;
            GlobalVar.key = "o7x8y6";
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                DialogResult result = MessageBox.Show("Do you really want to exit?", "Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                if (result == DialogResult.Yes)
                {
                    Process.GetCurrentProcess().Kill();
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void devicesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                Frmdevices devices = new Frmdevices();
                devices.MdiParent = this;
                devices.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MDImain_Load(object sender, EventArgs e)
        {
            MdiClient chld;

            foreach (Control ctrl in this.Controls)
            {
                try
                {
                    chld = (MdiClient)ctrl;
                    chld.BackColor = this.BackColor;
                }
                catch (InvalidCastException)
                {
                    
                }
            }
        }

        private void downloadAttlogsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAttLogsNew log = new FrmAttLogsNew();
            log.MdiParent = this;
            log.StartPosition = FormStartPosition.CenterScreen;
            log.Show();
        }

        private void licenseFileGenerateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSerialGenerate sln = new FrmSerialGenerate();
            sln.MdiParent = this;
            sln.Show();
        }

        private void faceUserManagementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmUserInfo face = new FrmUserInfo();
            face.MdiParent = this;
            face.StartPosition = FormStartPosition.CenterScreen;
            face.Show();
        }

        private void deleteUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmDelUser del = new FrmDelUser();
            del.MdiParent = this;
            del.Show();
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmChangePass pass = new FrmChangePass();
            pass.MdiParent = this;
            pass.Show();
        }
        protected void UpdateseriyalNumber()
        {
            try
            {
                string path = Path.Combine(Application.StartupPath, "Myla.SLN");
                var Lines = File.ReadLines(path);
                if (Lines != null)
                {
                    foreach (var line in Lines)
                    {
                        string readlinetext = line.ToString(); ;
                        string key, EnValue;
                        EnValue = Convert.ToString(readlinetext);
                        key = Convert.ToString(GlobalVar.key);
                        string convertserialnum = EncryptionandDecryption.DecryptStringAES(EnValue, key);
                        conn.Open();
                        string queryVal = "select DeviceSrNo from MyDevices where DeviceSrNo='" + convertserialnum + "'and EncryptSrNo is null ";
                        SqlCommand cmdval = new SqlCommand(queryVal, conn);
                        SqlDataAdapter da = new SqlDataAdapter(cmdval);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        if (dt.Rows.Count == 1)
                        {
                            conn.Close();
                            conn.Open();
                            string query = "update MyDevices set EncryptSrNo='" + EnValue.ToString() + "' where  DeviceSrNo='" + convertserialnum + "'";
                            SqlCommand cmd = new SqlCommand(query, conn);
                            cmd.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
            finally
            {
                conn.Close();
            }
        }

        private void updateLicenseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmLicenseUpdate li = new FrmLicenseUpdate();
            li.MdiParent = this;
            li.Show();
        }

        private void webLicenseToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void UserSearchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmEmpRecordSearch frmEmpRecordSearch = new FrmEmpRecordSearch();
            frmEmpRecordSearch.MdiParent = this;
            frmEmpRecordSearch.StartPosition = FormStartPosition.CenterScreen;
            frmEmpRecordSearch.Show();
        }
    }
}
