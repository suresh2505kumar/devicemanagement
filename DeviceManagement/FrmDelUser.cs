﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace DeviceManagement
{
    public partial class FrmDelUser : Form
    {
        public FrmDelUser()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand cmd;
        SqlDataAdapter da;
        DataTable dtUsers;
        public string DevId;
        public zkemkeeper.CZKEM axCZKEM1 = new zkemkeeper.CZKEM();
        private bool bIsConnected = false;//the boolean value identifies whether the device is connected
        private int iMachineNumber = 1;//the serial number of the device.After connecting the device ,this value will be changed.
        DataTable dt1 = new DataTable("Table1");
        DataTable dt2 = new DataTable();
        private void FrmDelUser_Load(object sender, EventArgs e)
        {
            txtKey.Text = "o7x8y6";
            Loadip();
            LoadTitle();
            LoadToTitle();
        }
        protected void Loadip()
        {
            try
            {
                conn.Open();
                string query = "select DeviceIp,DeviceSrNo,License from MyDevices where Active=1 and Location='" + GlobalVar.Branch + "'";
                cmd = new SqlCommand(query, conn);
                da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                cmd.Dispose();
                da.Dispose();
                chckIpList.DataSource = dt;
                chckIpList.ValueMember = "DeviceIp";
                chckIpList.DisplayMember = "DeviceIp";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }
        private void BtnConnect_Click(object sender, EventArgs e)
        {
            if (cmbip.Text == "" || txtPort.Text == "")
            {
                MessageBox.Show("IP and Port cannot be null", "Error");
                return;
            }
            int idwErrorCode = 0;
            Cursor = Cursors.WaitCursor;
            if (btnConnect.Text == "DisConnect")
            {
                axCZKEM1.Disconnect();
                bIsConnected = false;
                btnConnect.Text = "Connect";
                axCZKEM1.RegEvent(iMachineNumber, 65535);
                lblstate.Text = "Current Status is disconnected";
                lblstate.ForeColor = Color.Red;
                Cursor = Cursors.Default;
                return;
            }
            //axCZKEM1.PullMode = 1;
            bIsConnected = axCZKEM1.Connect_Net(cmbip.Text, Convert.ToInt32(txtPort.Text));
            if (bIsConnected == true)
            {
                btnConnect.Text = "DisConnect";
                btnConnect.Refresh();
                lblstate.Text = "Current State is:Connected";
                lblstate.ForeColor = Color.Green;
                iMachineNumber = 1;
                axCZKEM1.RegEvent(iMachineNumber, 65535);
            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");
            }
            Cursor = Cursors.Default;
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                string message = string.Empty;
                foreach (object item in chckIpList.CheckedItems)
                {
                    string ip = string.Empty;
                    DataRowView row = item as DataRowView;
                    ip = (string)row["DeviceIp"];
                    //MessageBox.Show(ip);
                    bIsConnected = axCZKEM1.Connect_Net(ip, Convert.ToInt32(4370));
                    for (int i = 0; i < DataGridToUsers.Rows.Count; i++)
                    {
                        int idwErrorCode = 0;
                        string sUserID = DataGridToUsers.Rows[i].Cells[1].Value.ToString();
                        int iBackupNumber = Convert.ToInt32(1);
                        Cursor = Cursors.WaitCursor;
                        if (axCZKEM1.SSR_DeleteEnrollData(iMachineNumber, sUserID, iBackupNumber))
                        {
                            axCZKEM1.RefreshData(iMachineNumber);//the data in the device should be refreshed
                        }
                        else
                        {
                            axCZKEM1.GetLastError(ref idwErrorCode);
                        }
                    }
                    axCZKEM1.EnableDevice(iMachineNumber, true);//enable the device
                    axCZKEM1.Disconnect();
                    bIsConnected = false;
                }
                Cursor = Cursors.Default;
                MessageBox.Show("Deleted EnrollData", "Success");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }
        private void CheckboxHeader_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvorginal.RowCount; i++)
            {
                dgvorginal[2, i].Value = ((CheckBox)dgvorginal.Controls.Find("checkboxHeader", true)[0]).Checked;
            }
            dgvorginal.EndEdit();
        }

        protected void LoadTitle()
        {
            dgvorginal.DataSource = null;
            dgvorginal.AutoGenerateColumns = false;
            dgvorginal.ColumnCount = 2;
            DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
            checkBoxColumn.HeaderText = "chck";
            checkBoxColumn.Width = 50;
            checkBoxColumn.Name = "checkBoxColumn";
            dgvorginal.Columns.Insert(0, checkBoxColumn);

            dgvorginal.Columns[1].Name = "UserId";
            dgvorginal.Columns[1].HeaderText = "UserId";

            dgvorginal.Columns[2].Name = "Name";
            dgvorginal.Columns[2].HeaderText = "Name";
            dgvorginal.Columns[2].Width = 140;
        }

        protected void LoadToTitle()
        {
            DataGridToUsers.DataSource = null;
            DataGridToUsers.AutoGenerateColumns = false;
            DataGridToUsers.ColumnCount = 2;
            DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
            checkBoxColumn.HeaderText = "chck";
            checkBoxColumn.Width = 50;
            checkBoxColumn.Name = "checkBoxColumn";
            DataGridToUsers.Columns.Insert(0, checkBoxColumn);

            DataGridToUsers.Columns[1].Name = "UserId";
            DataGridToUsers.Columns[1].HeaderText = "UserId";

            DataGridToUsers.Columns[2].Name = "Name";
            DataGridToUsers.Columns[2].HeaderText = "Name";
            DataGridToUsers.Columns[2].Width = 140;
        }

        protected DataTable LoadUsers()
        {
            DataTable dataTable = new DataTable();
            try
            {
                SQLDBHelper db = new SQLDBHelper();
                dataTable = db.getData("Proc_LoadUsers");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
            }
            return dataTable;
        }

        protected void FillDatagrid(DataTable dataTable)
        {
            try
            {
                if (dataTable.Rows.Count != 0)
                {
                    dgvorginal.Rows.Clear();
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        int rowId = dgvorginal.Rows.Add();
                        DataGridViewRow row = dgvorginal.Rows[rowId];
                        row.Cells[1].Value = dataTable.Rows[i]["User_Id"].ToString();
                        row.Cells[2].Value = dataTable.Rows[i]["Name"].ToString();
                    }
                }
                else
                {
                    MessageBox.Show("No Data Found", "Information");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
            }
        }


        private void BtnLoad_Click(object sender, EventArgs e)
        {
            try
            {
                dtUsers = LoadUsers();
                FillDatagrid(dtUsers);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void ChckSelectIp_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chckSelectIp.Checked == true)
                {
                    for (int i = 0; i < chckIpList.Items.Count; i++)
                    {
                        chckIpList.SetItemChecked(i, true);
                    }
                }
                else
                {
                    for (int i = 0; i < chckIpList.Items.Count; i++)
                    {
                        chckIpList.SetItemChecked(i, false);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void ChckSelectAllEmp_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chckSelectAllEmp.Checked == true)
                {
                    foreach (DataGridViewRow row in dgvorginal.Rows)
                    {
                        DataGridViewCheckBoxCell checkBox = (row.Cells["checkBoxColumn"] as DataGridViewCheckBoxCell);
                        checkBox.Value = true;
                    }
                }
                else
                {
                    foreach (DataGridViewRow row in dgvorginal.Rows)
                    {
                        DataGridViewCheckBoxCell checkBox = (row.Cells["checkBoxColumn"] as DataGridViewCheckBoxCell);
                        checkBox.Value = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnForward_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow dataGridRow in dgvorginal.Rows)
                {
                    if (dataGridRow.Cells["checkBoxColumn"].Value != null && (bool)dataGridRow.Cells["checkBoxColumn"].Value)
                    {
                        int rowId = DataGridToUsers.Rows.Add();
                        DataGridViewRow row = DataGridToUsers.Rows[rowId];
                        row.Cells[1].Value = dataGridRow.Cells["UserId"].Value.ToString();
                        row.Cells[2].Value = dataGridRow.Cells["Name"].Value.ToString();
                        dgvorginal.Rows.Remove(dataGridRow);
                        dgvorginal.ClearSelection();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnBackward_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow dataGridRow in DataGridToUsers.Rows)
                {
                    if (dataGridRow.Cells["checkBoxColumn"].Value != null && (bool)dataGridRow.Cells["checkBoxColumn"].Value)
                    {
                        int rowId = dgvorginal.Rows.Add();
                        DataGridViewRow row = dgvorginal.Rows[rowId];
                        row.Cells[1].Value = dataGridRow.Cells["UserId"].Value.ToString();
                        row.Cells[2].Value = dataGridRow.Cells["Name"].Value.ToString();
                        DataGridToUsers.Rows.Remove(dataGridRow);
                        DataGridToUsers.ClearSelection();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtUserid_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtUserid.Text != string.Empty)
                {
                    DataTable dataTable = dtUsers.Select("User_Id Like '%" + txtUserid.Text + "%'", "User_Id").CopyToDataTable();
                    FillDatagrid(dataTable);
                }
                else
                {
                    FillDatagrid(dtUsers);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
