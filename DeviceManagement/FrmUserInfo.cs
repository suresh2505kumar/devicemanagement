﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using zkemkeeper;

namespace DeviceManagement
{
    public partial class FrmUserInfo : Form
    {
        public FrmUserInfo()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SQLDBHelper db = new SQLDBHelper();
        public CZKEM axCZKEM1 = new CZKEM();
        private bool bIsConnected = false;//the boolean value identifies whether the device is connected
        private int iMachineNumber = 1;
        private static Int32 MyCount;
        private static Int32 MyCountFinger;
        private static Int32 MyCountFace;
        string Type = string.Empty;
        string TargetType = string.Empty;
        int LoadId = 0;
        private void chckSelectedUsers_CheckedChanged(object sender, EventArgs e)
        {
            if (chckFromDB.Checked == true)
            {
                if (chckFace.Checked == true)
                {
                    DataGridFaceFrom.Rows.Clear();
                    SqlParameter[] para = { new SqlParameter("@DeviceType", "IFACE Device") };
                    DataTable dt = db.getDataWithParameter("SP_GetUsersFromdevice", para);
                    if (dt.Rows.Count != 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DataGridViewRow row = (DataGridViewRow)DataGridFaceFrom.Rows[0].Clone();
                            row.Cells[1].Value = dt.Rows[i]["User_Id"].ToString();
                            row.Cells[2].Value = dt.Rows[i]["Name"].ToString();
                            row.Cells[3].Value = dt.Rows[i]["Passwords"].ToString();
                            row.Cells[4].Value = dt.Rows[i]["Privilege"].ToString();
                            row.Cells[5].Value = dt.Rows[i]["Face_Index"].ToString();
                            row.Cells[6].Value = dt.Rows[i]["Face_Image"].ToString();
                            row.Cells[7].Value = dt.Rows[i]["Face_Length"].ToString();
                            row.Cells[8].Value = dt.Rows[i]["Enabled"].ToString();
                            DataGridFaceFrom.Rows.Add(row);
                        }
                    }
                }
                else
                {
                    DataGridFaceFrom.Rows.Clear();
                    DataTable dt = db.getData("SP_GeTFTDevices");
                    if (dt.Rows.Count != 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DataGridViewRow row = (DataGridViewRow)DataGridFaceFrom.Rows[0].Clone();
                            row.Cells[1].Value = dt.Rows[i]["User_Id"].ToString();
                            row.Cells[2].Value = dt.Rows[i]["Name"].ToString();
                            row.Cells[3].Value = dt.Rows[i]["Passwords"].ToString();
                            row.Cells[6].Value = dt.Rows[i]["Finger_Image"].ToString();
                            row.Cells[5].Value = dt.Rows[i]["Finger_Index"].ToString();
                            row.Cells[4].Value = dt.Rows[i]["Privilege"].ToString();
                            row.Cells[7].Value = "";
                            row.Cells[8].Value = dt.Rows[i]["Enabled"].ToString();
                            DataGridFaceFrom.Rows.Add(row);
                        }
                    }
                }
            }
            else
            {
                DataGridFaceFrom.DataSource = null;
            }
        }

        private void chckFeSelectedUsers_CheckedChanged(object sender, EventArgs e)
        {
            if (chckFeSelectedUsers.Checked == true)
            {
                grFpBulk.Visible = false;
                grFpSelectedUsers.Visible = true;
            }
            else
            {
                grFpBulk.Visible = true;
                grFpSelectedUsers.Visible = false;
            }
        }

        private void FrmUserInfo_Load(object sender, EventArgs e)
        {
            GetDeviceIP();
            LoadFaceFromGrid();
            LoadFaceToGrid();
        }

        protected void GetDeviceIP()
        {
            try
            {
                DataTable data = db.getData("SP_GetDevices");
                DataTable dt = data.Select("Location='" + GlobalVar.Branch + "'", null).CopyToDataTable();
                if (dt.Rows.Count != 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        cmbip.Items.Add(dt.Rows[i]["Deviceip"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void BtnSource_Click(object sender, EventArgs e)
        {
            try
            {
                if (LoadId == 0)
                {
                    string SourceIp = cmbip.Text;
                    for (int i = 0; i < cmbip.Items.Count; i++)
                    {
                        cmbTargetIp.Items.Add(cmbip.Items[i].ToString());
                    }
                    cmbip.Items.Clear();
                    cmbip.Items.Add(SourceIp);
                    cmbTargetIp.SelectedIndex = 0;
                    cmbip.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void BtnConnectSourceIp_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbip.Text == "" || txtPort.Text == "")
                {
                    MessageBox.Show("IP and Port cannot be null", "Error");
                    return;
                }
                SqlParameter[] para = { new SqlParameter("@Deviceip", cmbip.Text) };
                DataTable dt = db.getDataWithParameter("SP_GetDeviceType", para);
                int idwErrorCode = 0;
                Type = dt.Rows[0]["DeviceType"].ToString();

                if (Type == "BW Device")
                {
                    if (btnConnectSourceIp.Text == "DisConnect")
                    {
                        axCZKEM1.Disconnect();
                        bIsConnected = false;
                        btnConnectSourceIp.Text = "Connect";
                        lblstate.Text = "Current State:DisConnected";
                        Cursor = Cursors.Default;
                        return;
                    }
                    if (btnConnectSourceIp.Text == "DisConnect")
                    {
                        axCZKEM1.Disconnect();
                        bIsConnected = false;
                        btnConnectSourceIp.Text = "Connect";
                        lblstate.Text = "Current State:DisConnected";
                        Cursor = Cursors.Default;
                        return;
                    }
                    LogFile.WriteLog("Macine Connection initiated " + cmbip.Text + " - " + DateTime.Now + "");
                    bIsConnected = axCZKEM1.Connect_Net(cmbip.Text, Convert.ToInt32(txtPort.Text));
                    if (bIsConnected == true)
                    {
                        LogFile.WriteLog("Macine Connected " + cmbip.Text + " - " + DateTime.Now + "");
                        btnConnectSourceIp.Text = "DisConnect";
                        btnConnectSourceIp.Refresh();
                        lblstate.Text = "Current State:Connected";
                        iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                        MyCount = 1;
                    }
                    else
                    {

                        axCZKEM1.GetLastError(ref idwErrorCode);
                        LogFile.WriteLog("Macine Not Connected " + cmbip.Text + " - " + idwErrorCode + DateTime.Now + "");
                        MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");
                    }
                    Cursor = Cursors.Default;
                }
                else if (Type == "TFT Device")
                {
                    if (btnConnectSourceIp.Text == "DisConnect")
                    {
                        axCZKEM1.Disconnect();
                        bIsConnected = false;
                        btnConnectSourceIp.Text = "Connect";
                        lblstate.Text = "Current State:DisConnected";
                        LogFile.WriteLog("Macine DisConnected " + cmbip.Text + " - " + DateTime.Now + "");
                        Cursor = Cursors.Default;
                        return;
                    }
                    LogFile.WriteLog("Macine Connection " + cmbip.Text + " - " + DateTime.Now + ", initiated");
                    bIsConnected = axCZKEM1.Connect_Net(cmbip.Text, Convert.ToInt32(txtPort.Text));
                    if (bIsConnected == true)
                    {
                        LogFile.WriteLog("Macine Connected " + cmbip.Text + " - " + DateTime.Now + "");
                        btnConnectSourceIp.Text = "DisConnect";
                        btnConnectSourceIp.Refresh();
                        lblstate.Text = "Current State:Connected";
                        iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                        MyCount = 1;
                    }
                    else
                    {
                        axCZKEM1.GetLastError(ref idwErrorCode);
                        LogFile.WriteLog("Macine Not Connected " + cmbip.Text + " - " + idwErrorCode + DateTime.Now + "");
                        MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");
                    }
                    Cursor = Cursors.Default;
                }
                else if (Type == "IFACE Device")
                {
                    Cursor = Cursors.WaitCursor;

                    if (btnConnectSourceIp.Text == "DisConnect")
                    {
                        axCZKEM1.Disconnect();
                        bIsConnected = false;
                        btnConnectSourceIp.Text = "Connect";
                        lblstate.Text = "Current State:DisConnected";
                        LogFile.WriteLog("Macine DisConnected " + cmbip.Text + " - " + DateTime.Now + "");
                        Cursor = Cursors.Default;
                        return;
                    }

                    //axCZKEM1.PullMode = 1;
                    LogFile.WriteLog("Macine Connection " + cmbip.Text + " - " + DateTime.Now + ", initiated");
                    bIsConnected = axCZKEM1.Connect_Net(cmbip.Text, Convert.ToInt32(txtPort.Text));
                    if (bIsConnected == true)
                    {
                        LogFile.WriteLog("Macine Connected " + cmbip.Text + " - " + DateTime.Now + "");
                        btnConnectSourceIp.Text = "DisConnect";
                        btnConnectSourceIp.Refresh();
                        lblstate.Text = "Current State:Connected";
                        iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                        axCZKEM1.RegEvent(iMachineNumber, 65535);//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
                        MyCountFinger = 1;
                        MyCountFace = 2;
                    }
                    else
                    {
                        axCZKEM1.GetLastError(ref idwErrorCode);
                        LogFile.WriteLog("Macine Not Connected " + cmbip.Text + " - " + idwErrorCode + DateTime.Now + "");
                        MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");
                    }
                    Cursor = Cursors.Default;
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void BtnDownloadUser_Click(object sender, EventArgs e)
        {
            try
            {
                if (Type == "BW Device")
                {

                }
                else if (Type == "TFT Device")
                {
                    if (MyCount == 1)
                    {
                        if (bIsConnected == false)
                        {
                            MessageBox.Show("Please connect the device first!", "Error");
                            return;
                        }
                        LogFile.WriteLog("User Downloading Started " + cmbip.Text + " - " + DateTime.Now + "");
                        string sName = "", sPassword = "", sTmpData = "", sdwEnrollNumber = "";
                        int iPrivilege = 0, idwFingerIndex = 0, iTmpLength = 0, iFlag = 0;
                        int ICount = 0;
                        bool bEnabled = false;
                        MyCount = -1;
                        DataGridFaceFrom.Rows.Clear();
                        axCZKEM1.EnableDevice(iMachineNumber, false);
                        Cursor = Cursors.WaitCursor;

                        axCZKEM1.ReadAllUserID(iMachineNumber);//read all the user information to the memory
                        axCZKEM1.ReadAllTemplate(iMachineNumber);//read all the users' fingerprint templates to the memory

                        while (axCZKEM1.SSR_GetAllUserInfo(iMachineNumber, out sdwEnrollNumber, out sName, out sPassword, out iPrivilege, out bEnabled))//get all the users' information from the memory
                        {
                            for (idwFingerIndex = 0; idwFingerIndex < 10; idwFingerIndex++)
                            {
                                if (axCZKEM1.GetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, out iFlag, out sTmpData, out iTmpLength))//get the corresponding templates string and length from the memory
                                {
                                    DataGridViewRow row = (DataGridViewRow)DataGridFaceFrom.Rows[0].Clone();
                                    row.Cells[1].Value = sdwEnrollNumber.ToString();
                                    row.Cells[2].Value = sName;
                                    row.Cells[3].Value = sPassword;
                                    row.Cells[6].Value = sTmpData;
                                    row.Cells[5].Value = idwFingerIndex;
                                    row.Cells[4].Value = iPrivilege.ToString();
                                    row.Cells[7].Value = iTmpLength;
                                    if (bEnabled == true)
                                    {
                                        row.Cells[8].Value = "true";
                                    }
                                    else
                                    {
                                        row.Cells[8].Value = "false";
                                    }
                                    DataGridFaceFrom.Rows.Add(row);
                                    // save in msaccess database
                                    SqlParameter[] parameters = {
                                        new SqlParameter("@userId", sdwEnrollNumber),
                                        new SqlParameter("@name", sName),
                                        new SqlParameter("@fingerIndex", idwFingerIndex),
                                        new SqlParameter("@fingerImage", sTmpData),
                                        new SqlParameter("@privilege", iPrivilege),
                                        new SqlParameter("@password", sPassword),
                                        new SqlParameter("@enabled", bEnabled),
                                        new SqlParameter("@flag", iFlag)
                                    };
                                    db.ExecuteQuery("SP_TFTDevice", parameters);
                                    ICount++;
                                }
                            }
                        }
                        axCZKEM1.EnableDevice(iMachineNumber, true);
                        LogFile.WriteLog("User Download Completed " + cmbip.Text + " - " + DateTime.Now + " TotalUsers: " + ICount + "");
                        Cursor = Cursors.Default;
                        MessageBox.Show("Records inserted successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else if (Type == "IFACE Device")
                {
                    if (MyCountFace == 2)
                    {
                        if (bIsConnected == false)
                        {
                            MessageBox.Show("Please connect the device first!", "Error");
                            return;
                        }
                        LogFile.WriteLog("User Downloading Started " + cmbip.Text + " - " + DateTime.Now + "");
                        string sUserID = "";
                        string sName = "";
                        string sPassword = "";
                        int iPrivilege = 0;
                        bool bEnabled = false;
                        int iFaceIndex = 50;//the only possible parameter value
                        string sTmpData = "";
                        int iLength = 0;
                        int ICount = 0;
                        MyCountFace = -2;
                        DataGridFaceFrom.Rows.Clear();
                        Cursor = Cursors.WaitCursor;
                        axCZKEM1.EnableDevice(iMachineNumber, false);
                        axCZKEM1.ReadAllUserID(iMachineNumber);//read all the user information to the memory
                        while (axCZKEM1.SSR_GetAllUserInfo(iMachineNumber, out sUserID, out sName, out sPassword, out iPrivilege, out bEnabled))//get all the users' information from the memory
                        {
                            if (axCZKEM1.GetUserFaceStr(iMachineNumber, sUserID, iFaceIndex, ref sTmpData, ref iLength))//get the face templates from the memory
                            {
                                DataGridViewRow row = (DataGridViewRow)DataGridFaceFrom.Rows[0].Clone();
                                row.Cells[1].Value = sUserID;
                                row.Cells[2].Value = sName;
                                row.Cells[3].Value = sPassword;
                                row.Cells[4].Value = iPrivilege.ToString();
                                row.Cells[5].Value = iFaceIndex.ToString();
                                row.Cells[6].Value = sTmpData;
                                row.Cells[7].Value = iLength.ToString();
                                if (bEnabled == true)
                                {
                                    row.Cells[8].Value = "true";
                                }
                                else
                                {
                                    row.Cells[8].Value = "false";
                                }
                                DataGridFaceFrom.Rows.Add(row);
                                // save in msaccess database
                                SqlParameter[] para = {
                                     new SqlParameter("@userId", sUserID),
                                     new SqlParameter("@name", sName),
                                     new SqlParameter("@password", sPassword),
                                     new SqlParameter("@privilege", iPrivilege.ToString()),
                                     new SqlParameter("@faceIndex", iFaceIndex.ToString()),
                                     new SqlParameter("@faceImage", sTmpData),
                                     new SqlParameter("@faceLength", iLength.ToString()),
                                     new SqlParameter("@enabled", bEnabled)
                                };
                                db.ExecuteQuery("SP_IFACEDevice_FaceTm", para);
                                ICount++;
                            }
                        }
                        axCZKEM1.EnableDevice(iMachineNumber, true);
                        LogFile.WriteLog("User Download Completed " + cmbip.Text + " - " + DateTime.Now + " TotalUsers: " + ICount + "");
                        Cursor = Cursors.Default;
                    }
                }
                else
                {
                    MessageBox.Show("Connect Device First", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        protected void LoadFaceFromGrid()
        {
            DataGridFaceFrom.DataSource = null;
            DataGridFaceFrom.AutoGenerateColumns = false;
            DataGridFaceFrom.ColumnCount = 8;

            DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
            checkBoxColumn.HeaderText = "chck";
            checkBoxColumn.Width = 50;
            checkBoxColumn.Name = "checkBoxColumn";
            DataGridFaceFrom.Columns.Insert(0, checkBoxColumn);

            DataGridFaceFrom.Columns[1].Name = "UserId";
            DataGridFaceFrom.Columns[1].HeaderText = "UserId";

            DataGridFaceFrom.Columns[2].Name = "Name";
            DataGridFaceFrom.Columns[2].HeaderText = "Name";
            DataGridFaceFrom.Columns[2].Width = 160;

            DataGridFaceFrom.Columns[3].Name = "Password";
            DataGridFaceFrom.Columns[3].HeaderText = "Password";
            DataGridFaceFrom.Columns[3].Visible = false;

            DataGridFaceFrom.Columns[4].Name = "Privilege";
            DataGridFaceFrom.Columns[4].HeaderText = "Privilege";
            DataGridFaceFrom.Columns[4].Visible = false;

            DataGridFaceFrom.Columns[5].Name = "FaceIndex";
            DataGridFaceFrom.Columns[5].HeaderText = "FaceIndex";
            DataGridFaceFrom.Columns[5].Visible = false;

            DataGridFaceFrom.Columns[6].Name = "TmpData";
            DataGridFaceFrom.Columns[6].HeaderText = "TmpData";
            DataGridFaceFrom.Columns[6].Visible = false;

            DataGridFaceFrom.Columns[7].Name = "Length";
            DataGridFaceFrom.Columns[7].HeaderText = "Length";
            DataGridFaceFrom.Columns[7].Visible = false;

            DataGridFaceFrom.Columns[8].Name = "Enabled";
            DataGridFaceFrom.Columns[8].HeaderText = "Enabled";
            DataGridFaceFrom.Columns[8].Visible = false;
        }

        protected void LoadFaceToGrid()
        {
            DataGridFaceTo.DataSource = null;
            DataGridFaceTo.AutoGenerateColumns = false;
            DataGridFaceTo.ColumnCount = 8;

            DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
            checkBoxColumn.HeaderText = "chck";
            checkBoxColumn.Width = 50;
            checkBoxColumn.Name = "checkBoxColumn";
            DataGridFaceTo.Columns.Insert(0, checkBoxColumn);

            DataGridFaceTo.Columns[1].Name = "UserId";
            DataGridFaceTo.Columns[1].HeaderText = "UserId";

            DataGridFaceTo.Columns[2].Name = "Name";
            DataGridFaceTo.Columns[2].HeaderText = "Name";
            DataGridFaceTo.Columns[2].Width = 150;

            DataGridFaceTo.Columns[3].Name = "Password";
            DataGridFaceTo.Columns[3].HeaderText = "Password";
            DataGridFaceTo.Columns[3].Visible = false;

            DataGridFaceTo.Columns[4].Name = "Privilege";
            DataGridFaceTo.Columns[4].HeaderText = "Privilege";
            DataGridFaceTo.Columns[4].Visible = false;

            DataGridFaceTo.Columns[5].Name = "FaceIndex";
            DataGridFaceTo.Columns[5].HeaderText = "FaceIndex";
            DataGridFaceTo.Columns[5].Visible = false;

            DataGridFaceTo.Columns[6].Name = "TmpData";
            DataGridFaceTo.Columns[6].HeaderText = "TmpData";
            DataGridFaceTo.Columns[6].Visible = false;

            DataGridFaceTo.Columns[7].Name = "Length";
            DataGridFaceTo.Columns[7].HeaderText = "Length";
            DataGridFaceTo.Columns[7].Visible = false;

            DataGridFaceTo.Columns[8].Name = "Enabled";
            DataGridFaceTo.Columns[8].HeaderText = "Enabled";
            DataGridFaceTo.Columns[8].Visible = false;
        }

        private void cmbip_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbTargetIp.Text == "" || txtPort.Text == "")
                {
                    MessageBox.Show("IP and Port cannot be null", "Error");
                    return;
                }
                SqlParameter[] para = { new SqlParameter("@Deviceip", cmbTargetIp.Text) };
                DataTable dt = db.getDataWithParameter("SP_GetDeviceType", para);
                int idwErrorCode = 0;
                Type = dt.Rows[0]["DeviceType"].ToString();

                if (Type == "BW Device")
                {
                    if (btnConnectSourceIp.Text == "DisConnect")
                    {
                        axCZKEM1.Disconnect();
                        bIsConnected = false;
                        btnConnectSourceIp.Text = "Connect";
                        lblstate.Text = "Current State:DisConnected";
                        Cursor = Cursors.Default;
                        return;
                    }
                    if (btnConnectSourceIp.Text == "DisConnect")
                    {
                        axCZKEM1.Disconnect();
                        bIsConnected = false;
                        btnConnectSourceIp.Text = "Connect";
                        lblstate.Text = "Current State:DisConnected";
                        Cursor = Cursors.Default;
                        return;
                    }
                    bIsConnected = axCZKEM1.Connect_Net(cmbTargetIp.Text, Convert.ToInt32(txtPort.Text));
                    if (bIsConnected == true)
                    {
                        btnConnectSourceIp.Text = "DisConnect";
                        btnConnectSourceIp.Refresh();
                        lblstate.Text = "Current State:Connected";
                        iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                        MyCount = 1;
                    }
                    else
                    {
                        axCZKEM1.GetLastError(ref idwErrorCode);
                        MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");
                    }
                    Cursor = Cursors.Default;
                }
                else if (Type == "TFT Device")
                {
                    if (btnConnectSourceIp.Text == "DisConnect")
                    {
                        axCZKEM1.Disconnect();
                        bIsConnected = false;
                        btnConnectSourceIp.Text = "Connect";
                        lblstate.Text = "Current State:DisConnected";
                        Cursor = Cursors.Default;
                        return;
                    }
                    bIsConnected = axCZKEM1.Connect_Net(cmbTargetIp.Text, Convert.ToInt32(txtPort.Text));
                    if (bIsConnected == true)
                    {
                        btnConnectSourceIp.Text = "DisConnect";
                        btnConnectSourceIp.Refresh();
                        lblstate.Text = "Current State:Connected";
                        iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                        MyCount = 1;
                    }
                    else
                    {
                        axCZKEM1.GetLastError(ref idwErrorCode);
                        MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");
                    }
                    Cursor = Cursors.Default;
                }
                else if (Type == "IFACE Device")
                {
                    Cursor = Cursors.WaitCursor;

                    if (btnConnectSourceIp.Text == "DisConnect")
                    {
                        axCZKEM1.Disconnect();
                        bIsConnected = false;
                        btnConnectSourceIp.Text = "Connect";
                        lblstate.Text = "Current State:DisConnected";
                        Cursor = Cursors.Default;
                        return;
                    }

                    //axCZKEM1.PullMode = 1;
                    bIsConnected = axCZKEM1.Connect_Net(cmbTargetIp.Text, Convert.ToInt32(txtPort.Text));
                    if (bIsConnected == true)
                    {
                        btnConnectSourceIp.Text = "DisConnect";
                        btnConnectSourceIp.Refresh();
                        lblstate.Text = "Current State:Connected";
                        iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                        axCZKEM1.RegEvent(iMachineNumber, 65535);//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
                        MyCountFinger = 1;
                        MyCountFace = 2;
                    }
                    else
                    {
                        axCZKEM1.GetLastError(ref idwErrorCode);
                        MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");
                    }
                    Cursor = Cursors.Default;
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btnFaceUploadSelectedUsers_Click(object sender, EventArgs e)
        {
            try
            {
                if (bIsConnected == false)
                {
                    MessageBox.Show("Please connect the device first!", "Error");
                    return;
                }
                if (Type == "BW Device")
                {
                }
                else if (Type == "TFT Device")
                {
                    if (bIsConnected == false)
                    {
                        MessageBox.Show("Please connect the device first!", "Error");
                        return;
                    }

                    int idwErrorCode = 0;
                    string sdwEnrollNumber = "";
                    string sName = "";
                    int idwFingerIndex = 0;
                    string sTmpData = "";
                    int iPrivilege = 0;
                    string sPassword = "";
                    string sEnabled = "";
                    string sLastEnrollNumber = "";

                    Cursor = Cursors.WaitCursor;
                    axCZKEM1.EnableDevice(iMachineNumber, false);

                    // select data from database
                    //UICO objupload = new UICO();
                    DataTable dt = new DataTable();
                    string upload_data = "SELECT * FROM  TFTDevice";
                    SqlCommand command = new SqlCommand(upload_data, conn);
                    SqlDataAdapter da = new SqlDataAdapter(command);
                    da.Fill(dt);
                    // start select data from database to upload in listview
                    foreach (DataGridViewRow dataGridRow in DataGridFaceTo.Rows)
                    {
                        if (dataGridRow.Cells["checkBoxColumn"].Value != null && (bool)dataGridRow.Cells["checkBoxColumn"].Value)
                        {
                            //sdwEnrollNumber = string.IsNullOrEmpty(dt.Rows[i]["User_Id"].ToString()) ? " " : dt.Rows[i]["User_Id"].ToString();
                            //sName = string.IsNullOrEmpty(dt.Rows[i]["Name"].ToString()) ? " " : dt.Rows[i]["Name"].ToString();
                            //idwFingerIndex = string.IsNullOrEmpty(dt.Rows[i]["Finger_Index"].ToString()) ? 0 : Convert.ToInt32(dt.Rows[i]["Finger_Index"].ToString());
                            //sTmpData = string.IsNullOrEmpty(dt.Rows[i]["Finger_Image"].ToString()) ? " " : dt.Rows[i]["Finger_Image"].ToString();
                            //iPrivilege = string.IsNullOrEmpty(dt.Rows[i]["Privilege"].ToString()) ? 0 : Convert.ToInt32(dt.Rows[i]["Privilege"].ToString());
                            //sPassword = string.IsNullOrEmpty(dt.Rows[i]["Passwords"].ToString()) ? null : dt.Rows[i]["Passwords"].ToString();
                            //sEnabled = string.IsNullOrEmpty(dt.Rows[i]["Enabled"].ToString()) ? " " : dt.Rows[i]["Enabled"].ToString();
                            //int iFlag = Convert.ToInt32(dt.Rows[i]["Flag"].ToString());
                            sdwEnrollNumber = dataGridRow.Cells[1].Value.ToString();
                            sName = dataGridRow.Cells[2].Value.ToString();
                            idwFingerIndex = Convert.ToInt32(dataGridRow.Cells[5].Value.ToString());
                            sTmpData = dataGridRow.Cells[6].Value.ToString();
                            iPrivilege = Convert.ToInt32(dataGridRow.Cells[4].Value.ToString());
                            sPassword = dataGridRow.Cells[3].Value.ToString();
                            sEnabled = dataGridRow.Cells[8].Value.ToString();
                            int iFlag = Convert.ToInt32(1);

                            if (sdwEnrollNumber != sLastEnrollNumber)//identify whether the user information(except fingerprint templates) has been uploaded
                            {
                                if (axCZKEM1.SSR_SetUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, Convert.ToBoolean(sEnabled)))//upload user information to the memory
                                {
                                    axCZKEM1.SetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, iFlag, sTmpData);//upload templates information to the memory
                                }
                                else
                                {
                                    axCZKEM1.GetLastError(ref idwErrorCode);
                                    MessageBox.Show("Operation failed,ErrorCode=" + idwErrorCode.ToString(), "Error");
                                    Cursor = Cursors.Default;
                                    axCZKEM1.EnableDevice(iMachineNumber, true);
                                    return;
                                }
                            }
                            else//the current fingerprint and the former one belongs the same user,that is ,one user has more than one template
                            {
                                axCZKEM1.SetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, iFlag, sTmpData);
                            }
                            sLastEnrollNumber = sdwEnrollNumber;//change the value of iLastEnrollNumber dynamicly
                        }
                    }
                    //end
                    axCZKEM1.RefreshData(iMachineNumber);//the data in the device should be refreshed
                    Cursor = Cursors.Default;
                    axCZKEM1.EnableDevice(iMachineNumber, true);
                    MessageBox.Show("Successfully upload fingerprint templates , " + "total:" + dt.Rows.Count.ToString(), "Success");
                }
                else if (Type == "IFACE Device")
                {
                    int idwErrorCode = 0;
                    string sUserID = "";
                    string sName = "";
                    int iFaceIndex = 0;
                    string sTmpData = "";
                    int iLength = 0;
                    int iPrivilege = 0;
                    string sPassword = "";
                    string sEnabled = "";
                    bool bEnabled = false;


                    Cursor = Cursors.WaitCursor;
                    axCZKEM1.EnableDevice(iMachineNumber, false);
                    foreach (DataGridViewRow dataGridRow in DataGridFaceTo.Rows)
                    {
                        if (dataGridRow.Cells["checkBoxColumn"].Value != null && (bool)dataGridRow.Cells["checkBoxColumn"].Value)
                        {
                            sUserID = dataGridRow.Cells[1].Value.ToString();
                            sName = dataGridRow.Cells[2].Value.ToString();
                            sPassword = dataGridRow.Cells[3].Value.ToString();
                            iPrivilege = Convert.ToInt32(dataGridRow.Cells[4].Value.ToString());
                            iFaceIndex = Convert.ToInt32(dataGridRow.Cells[5].Value.ToString());
                            sTmpData = dataGridRow.Cells[6].Value.ToString();
                            iLength = Convert.ToInt32(dataGridRow.Cells[7].Value.ToString());
                            bEnabled = Convert.ToBoolean(dataGridRow.Cells[8].Value.ToString());
                            if (sEnabled == "true")
                            {
                                bEnabled = true;
                            }
                            else
                            {
                                bEnabled = false;
                            }

                            if (axCZKEM1.SSR_SetUserInfo(iMachineNumber, sUserID, sName, sPassword, iPrivilege, bEnabled))//face templates are part of users' information
                            {
                                axCZKEM1.SetUserFaceStr(iMachineNumber, sUserID, iFaceIndex, sTmpData, iLength);//upload face templates information to the device
                            }
                            else
                            {
                                axCZKEM1.GetLastError(ref idwErrorCode);
                                MessageBox.Show("Operation failed,ErrorCode=" + idwErrorCode.ToString(), "Error");
                                Cursor = Cursors.Default;
                                axCZKEM1.EnableDevice(iMachineNumber, true);
                                return;
                            }
                        }
                    }
                    axCZKEM1.RefreshData(iMachineNumber);//the data in the device should be refreshed
                    Cursor = Cursors.Default;
                    axCZKEM1.EnableDevice(iMachineNumber, true);
                    MessageBox.Show("Successfully Upload the face templates", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btnFaceForward_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow dataGridRow in DataGridFaceFrom.Rows)
                {
                    if (dataGridRow.Cells["checkBoxColumn"].Value != null && (bool)dataGridRow.Cells["checkBoxColumn"].Value)
                    {
                        DataGridViewRow row = (DataGridViewRow)DataGridFaceTo.Rows[0].Clone();
                        row.Cells[1].Value = dataGridRow.Cells[1].Value;
                        row.Cells[2].Value = dataGridRow.Cells[2].Value;
                        row.Cells[3].Value = dataGridRow.Cells[3].Value;
                        row.Cells[4].Value = dataGridRow.Cells[4].Value;
                        row.Cells[5].Value = dataGridRow.Cells[5].Value;
                        row.Cells[6].Value = dataGridRow.Cells[6].Value;
                        row.Cells[7].Value = dataGridRow.Cells[7].Value;
                        row.Cells[8].Value = dataGridRow.Cells[8].Value;
                        DataGridFaceTo.Rows.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (checkBox1.Checked == true)
                {
                    chckFace.Checked = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void chckFace_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chckFace.Checked == true)
                {
                    checkBox1.Checked = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void txtEmpCodeSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataGridFaceFrom.Rows.Clear();
                SqlParameter[] para = { new SqlParameter("@Searchdata", txtEmpCodeSearch.Text) };
                if (chckFace.Checked == true)
                {
                    DataGridFaceFrom.Rows.Clear();
                    DataTable dt = db.getDataWithParameter("SP_GetFacesearch", para);
                    if (dt.Rows.Count != 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DataGridViewRow row = (DataGridViewRow)DataGridFaceFrom.Rows[0].Clone();
                            row.Cells[1].Value = dt.Rows[i]["User_Id"].ToString();
                            row.Cells[2].Value = dt.Rows[i]["Name"].ToString();
                            row.Cells[3].Value = dt.Rows[i]["Passwords"].ToString();
                            row.Cells[4].Value = dt.Rows[i]["Privilege"].ToString();
                            row.Cells[5].Value = dt.Rows[i]["Face_Index"].ToString();
                            row.Cells[6].Value = dt.Rows[i]["Face_Image"].ToString();
                            row.Cells[7].Value = dt.Rows[i]["Face_Length"].ToString();
                            row.Cells[8].Value = dt.Rows[i]["Enabled"].ToString();
                            DataGridFaceFrom.Rows.Add(row);
                        }
                    }
                }
                else
                {
                    DataTable dt = db.getDataWithParameter("SP_getTftSearch", para);
                    if (dt.Rows.Count != 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DataGridViewRow row = (DataGridViewRow)DataGridFaceFrom.Rows[0].Clone();
                            row.Cells[1].Value = dt.Rows[i]["User_Id"].ToString();
                            row.Cells[2].Value = dt.Rows[i]["Name"].ToString();
                            row.Cells[3].Value = dt.Rows[i]["Passwords"].ToString();
                            row.Cells[6].Value = dt.Rows[i]["Finger_Image"].ToString();
                            row.Cells[5].Value = dt.Rows[i]["Finger_Index"].ToString();
                            row.Cells[4].Value = dt.Rows[i]["Privilege"].ToString();
                            row.Cells[7].Value = "";
                            row.Cells[8].Value = dt.Rows[i]["Enabled"].ToString();
                            DataGridFaceFrom.Rows.Add(row);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}
