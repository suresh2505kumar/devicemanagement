﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeviceManagement
{
    public partial class FrmLicenseUpdate : Form
    {
        public FrmLicenseUpdate()
        {
            InitializeComponent();
        }

        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlConnection connWeb = new SqlConnection("Data Source=anemone.arvixe.com; Initial Catalog=MepAdmin;User Id=MepUser01;password=CounterPay01");
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                string query = "select DeviceSrNo from MyDevices where License is null and Active=1";
                SqlCommand cmd = new SqlCommand(query, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string devsno = dt.Rows[i]["DeviceSrNo"].ToString();
                    connWeb.Close();
                    connWeb.Open();
                    string queryWeb = "select DeviceSNo,LicenseKey from LicenseKey where LicenseKey is not null and DeviceSNo='" + devsno + "' ";
                    SqlCommand cmdweb = new SqlCommand(queryWeb, connWeb);
                    SqlDataAdapter daweb = new SqlDataAdapter(cmdweb);
                    DataTable dtweb = new DataTable();
                    daweb.Fill(dtweb);
                    if (dtweb.Rows.Count == 1)
                    {
                        conn.Close();
                        conn.Open();
                        string websno = dtweb.Rows[0]["DeviceSNo"].ToString();
                        string license = dtweb.Rows[0]["LicenseKey"].ToString();
                        conn.Close();
                        conn.Open();
                        string updateqry = "update MyDevices set License ='"+license+"' where DeviceSrNo='"+websno+"'";
                        SqlCommand mcdupdate = new SqlCommand(updateqry, conn);
                        mcdupdate.ExecuteNonQuery();
                        conn.Close();
                    }
                }

                MessageBox.Show("Updated","Sucess");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
            finally
            {
                conn.Close();
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
