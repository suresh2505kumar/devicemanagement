﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using zkemkeeper;
using System.Security;
using System.Security.Cryptography;
using System.Runtime;
namespace DeviceManagement
{
    public partial class FrmFaceManagement : Form
    {
        public FrmFaceManagement()
        {
            InitializeComponent();
        }

        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand cmd;
        SqlDataAdapter da;
        public string DevId;
        public zkemkeeper.CZKEM axCZKEM1 = new zkemkeeper.CZKEM();
        private bool bIsConnected = false;//the boolean value identifies whether the device is connected
        private int iMachineNumber = 1;//the serial number of the device.After connecting the device ,this value will be changed.
       
        private static int MyCountFace;
        private void FrmFaceManagement_Load(object sender, EventArgs e)
        {
            txtKey.Text = "o7x8y6";
            loadip();
            
        }
        protected void loadip()
        {
            try
            {
                conn.Open();
                string query = "select DeviceIp,DeviceSrNo,License from MyDevices where license is not null and Active=1";
                cmd = new SqlCommand(query, conn);
                da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                cmd.Dispose();
                da.Dispose();
                int i = 0;
                for (i = 0; i < dt.Rows.Count; i++)
                {

                    //string path = Path.Combine(Application.StartupPath, "Myla.SLN");
                    //var Lines = File.ReadLines(path);
                    //if (Lines != null)
                    // {
                    //  foreach (var line in Lines)
                    // {
                    string readlinetext = dt.Rows[i]["License"].ToString();
                    string originalSno = dt.Rows[i]["DeviceSrNo"].ToString();
                    string key, EnValue;
                    EnValue = Convert.ToString(readlinetext);
                    key = Convert.ToString(txtKey.Text);
                    string convertserialnum = EncryptionandDecryption.DecryptStringAES(EnValue, key);
                    //for (i = 0; i < dt.Rows.Count; i++)
                    if (convertserialnum == originalSno)
                    {
                        string orginalseriayal = dt.Rows[i]["DeviceSrNo"].ToString();
                        if (orginalseriayal == convertserialnum)
                        {
                            cmbip.Items.Add(dt.Rows[i]["DeviceIp"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }       
        private void btnDownLoadFace_Click(object sender, EventArgs e)
        {

            if (MyCountFace == 2)
            {
                if (bIsConnected == false)
                {
                    MessageBox.Show("Please connect the device first!", "Error");
                    return;
                }

                string sUserID = "";
                string sName = "";
                string sPassword = "";
                int iPrivilege = 0;
                bool bEnabled = false;

                int iFaceIndex = 50;//the only possible parameter value
                string sTmpData = "";
                int iLength = 0;

                MyCountFace = -2;

                //lvFace.Items.Clear();
                // lvFace.BeginUpdate();

                Cursor = Cursors.WaitCursor;
                axCZKEM1.EnableDevice(iMachineNumber, false);
                axCZKEM1.ReadAllUserID(iMachineNumber);//read all the user information to the memory                
                while (axCZKEM1.SSR_GetAllUserInfo(iMachineNumber, out sUserID, out sName, out sPassword, out iPrivilege, out bEnabled))//get all the users' information from the memory
                {
                    if (axCZKEM1.GetUserFaceStr(iMachineNumber, sUserID, iFaceIndex, ref sTmpData, ref iLength))//get the face templates from the memory
                    {
                        DataTable dt1 = new DataTable("table");

                        DataRow dr = dt1.NewRow();

                        ListViewItem list = new ListViewItem();
                        list.Text = sUserID;
                        list.SubItems.Add(sName);
                        list.SubItems.Add(sPassword);
                        list.SubItems.Add(iPrivilege.ToString());
                        list.SubItems.Add(iFaceIndex.ToString());
                        list.SubItems.Add(sTmpData);
                        list.SubItems.Add(iLength.ToString());
                        if (bEnabled == true)
                        {
                            list.SubItems.Add("true");
                        }
                        else
                        {
                            list.SubItems.Add("false");
                        }
                        lvFace.Items.Add(list);

                        // save in msaccess database 
                        conn.Open();
                        string val = "select * from IFACEDevice_FaceTm where User_Id='" + sUserID + "'";
                        SqlCommand valcmd = new SqlCommand(val, conn);
                        SqlDataAdapter da = new SqlDataAdapter(valcmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        if (dt.Rows.Count != 0)
                        {
                            conn.Close();
                            conn.Open();
                            string querydel = "delete from IFACEDevice_FaceTm where User_Id='" + sUserID + "'";
                            SqlCommand cmddel = new SqlCommand(querydel, conn);
                            cmddel.ExecuteNonQuery();
                        }
                        conn.Close();
                        conn.Open();
                        string query = "INSERT INTO [IFACEDevice_FaceTm](User_Id,Name,Passwords,Privilege,Face_Index,Face_Image,Face_Length,Enabled) VALUES(@userId,@name,@password,@privilege,@faceIndex,@faceImage,@faceLength,@enabled)";
                        SqlCommand cmd = new SqlCommand(query, conn);
                        cmd.Parameters.AddWithValue("@userId", sUserID);
                        cmd.Parameters.AddWithValue("@name", sName);
                        cmd.Parameters.AddWithValue("@password", sPassword);
                        cmd.Parameters.AddWithValue("@privilege", iPrivilege);
                        cmd.Parameters.AddWithValue("@faceIndex", iFaceIndex);
                        cmd.Parameters.AddWithValue("@faceImage", sTmpData);
                        cmd.Parameters.AddWithValue("@faceLength", iLength);
                        cmd.Parameters.AddWithValue("@enabled", bEnabled);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                      
                    }
                }
                axCZKEM1.EnableDevice(iMachineNumber, true);
                lvFace.EndUpdate();
                conn.Close();
                Cursor = Cursors.Default;
                MessageBox.Show("Completed");
            }
        }

        private void btnUploadFace_Click(object sender, EventArgs e)
        {
            if (bIsConnected == false)
            {
                MessageBox.Show("Please connect the device first!", "Error");
                return;
            }
            int idwErrorCode = 0;

            string sUserID = "";
            string sName = "";
            int iFaceIndex = 0;
            string sTmpData = "";
            int iLength = 0;
            int iPrivilege = 0;
            string sPassword = "";
            string sEnabled = "";
            bool bEnabled = false;
            string lastUserId = "";

            Cursor = Cursors.WaitCursor;
            axCZKEM1.EnableDevice(iMachineNumber, false);

            // select data from database           
            DataTable dt = new DataTable();
            conn.Open();
            string upload_data = "SELECT * FROM  IFACEDevice_FaceTm order by User_id";
            SqlCommand cmd = new SqlCommand(upload_data, conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            lvFace.Items.Clear();
            lvFace.BeginUpdate();
            for (int j = 0; j < dt.Rows.Count; j++)
            {
                sUserID = string.IsNullOrEmpty(dt.Rows[j]["User_Id"].ToString()) ? " " : dt.Rows[j]["User_Id"].ToString();
                sName = string.IsNullOrEmpty(dt.Rows[j]["Name"].ToString()) ? " " : dt.Rows[j]["Name"].ToString();
                sPassword = string.IsNullOrEmpty(dt.Rows[j]["Passwords"].ToString()) ? null : dt.Rows[j]["Passwords"].ToString();
                iPrivilege = string.IsNullOrEmpty(dt.Rows[j]["Privilege"].ToString()) ? 0 : Convert.ToInt32(dt.Rows[j]["Privilege"].ToString());
                iFaceIndex = string.IsNullOrEmpty(dt.Rows[j]["Face_Index"].ToString()) ? 0 : Convert.ToInt32(dt.Rows[j]["Face_Index"].ToString());
                sTmpData = string.IsNullOrEmpty(dt.Rows[j]["Face_Image"].ToString()) ? " " : dt.Rows[j]["Face_Image"].ToString();
                iLength = string.IsNullOrEmpty(dt.Rows[j]["Face_Length"].ToString()) ? 0 : Convert.ToInt32(dt.Rows[j]["Face_Length"].ToString());
                sEnabled = string.IsNullOrEmpty(dt.Rows[j]["Enabled"].ToString()) ? " " : dt.Rows[j]["Enabled"].ToString();
                ListViewItem list = new ListViewItem();
                list.Text = sUserID;
                list.SubItems.Add(sName);
                list.SubItems.Add(sPassword);
                list.SubItems.Add(iPrivilege.ToString());
                list.SubItems.Add(iFaceIndex.ToString());
                list.SubItems.Add(sTmpData);
                list.SubItems.Add(iLength.ToString());
                if (bEnabled == true)
                {
                    list.SubItems.Add("true");
                }
                else
                {
                    list.SubItems.Add("false");
                }
                lvFace.Items.Add(list);
            }
            lvFace.EndUpdate();
            DialogResult result = MessageBox.Show("Dou Want Upload All Users to Machine", "Information", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                // start select data from database to upload in listview
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sUserID = string.IsNullOrEmpty(dt.Rows[i]["User_Id"].ToString()) ? " " : dt.Rows[i]["User_Id"].ToString();
                    sName = string.IsNullOrEmpty(dt.Rows[i]["Name"].ToString()) ? " " : dt.Rows[i]["Name"].ToString();
                    sPassword = string.IsNullOrEmpty(dt.Rows[i]["Passwords"].ToString()) ? null : dt.Rows[i]["Passwords"].ToString();
                    iPrivilege = string.IsNullOrEmpty(dt.Rows[i]["Privilege"].ToString()) ? 0 : Convert.ToInt32(dt.Rows[i]["Privilege"].ToString());
                    iFaceIndex = string.IsNullOrEmpty(dt.Rows[i]["Face_Index"].ToString()) ? 0 : Convert.ToInt32(dt.Rows[i]["Face_Index"].ToString());
                    sTmpData = string.IsNullOrEmpty(dt.Rows[i]["Face_Image"].ToString()) ? " " : dt.Rows[i]["Face_Image"].ToString();
                    iLength = string.IsNullOrEmpty(dt.Rows[i]["Face_Length"].ToString()) ? 0 : Convert.ToInt32(dt.Rows[i]["Face_Length"].ToString());
                    sEnabled = string.IsNullOrEmpty(dt.Rows[i]["Enabled"].ToString()) ? " " : dt.Rows[i]["Enabled"].ToString();

                    if (sUserID != lastUserId)//identify whether the user information(except fingerprint templates) has been uploaded
                    {
                        if (axCZKEM1.SSR_SetUserInfo(iMachineNumber, sUserID, sName, sPassword, iPrivilege, Convert.ToBoolean(sEnabled)))//upload user information to the memory
                        {
                            axCZKEM1.SetUserFaceStr(iMachineNumber, sUserID, iFaceIndex, sTmpData, iLength);//upload templates information to the memory
                        }
                        else
                        {
                            axCZKEM1.GetLastError(ref idwErrorCode);
                            MessageBox.Show("Operation failed,ErrorCode=" + idwErrorCode.ToString(), "Error");
                            Cursor = Cursors.Default;
                            axCZKEM1.EnableDevice(iMachineNumber, true);
                            return;
                        }
                    }
                    else//the current fingerprint and the former one belongs the same user,that is ,one user has more than one template
                    {
                        axCZKEM1.SetUserFaceStr(iMachineNumber, sUserID, iFaceIndex, sTmpData, iLength);
                    }
                    sUserID = lastUserId;//change the value of userId dynamicly
                    MessageBox.Show("Successfully Upload the face templates, " + "total:" + dt.Rows.Count.ToString(), "Success");
                }
                //end                           

                axCZKEM1.RefreshData(iMachineNumber);//the data in the device should be refreshed
                Cursor = Cursors.Default;
                axCZKEM1.EnableDevice(iMachineNumber, true);
                
            }
            else
            {
                axCZKEM1.RefreshData(iMachineNumber);//the data in the device should be refreshed
                Cursor = Cursors.Default;
                axCZKEM1.EnableDevice(iMachineNumber, true);
            }
        }

        private void btnUpldUsers_Click(object sender, EventArgs e)
        {
            try
            {
                if (bIsConnected == false)
                {
                    MessageBox.Show("Please connect the device first!", "Error");
                    return;
                }
                int idwErrorCode = 0;

                string sUserID = "";
                string sName = "";
                int iFaceIndex = 0;
                string sTmpData = "";
                int iLength = 0;
                int iPrivilege = 0;
                string sPassword = "";
                string sEnabled = "";
                bool bEnabled = false;
                string lastUserId = "";

                Cursor = Cursors.WaitCursor;
                axCZKEM1.EnableDevice(iMachineNumber, false);
                foreach (ListViewItem checkedItem in lvFace.CheckedItems)
                {
                    sUserID = checkedItem.SubItems[0].Text;

                    string query = "select * from IFACEDevice_FaceTm where User_Id='" + sUserID + "'";
                    conn.Close();
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(query, conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    if (dt.Rows.Count == 1)
                    {
                        sUserID = string.IsNullOrEmpty(dt.Rows[0]["User_Id"].ToString()) ? " " : dt.Rows[0]["User_Id"].ToString();
                        sName = string.IsNullOrEmpty(dt.Rows[0]["Name"].ToString()) ? " " : dt.Rows[0]["Name"].ToString();
                        sPassword = string.IsNullOrEmpty(dt.Rows[0]["Passwords"].ToString()) ? null : dt.Rows[0]["Passwords"].ToString();
                        iPrivilege = string.IsNullOrEmpty(dt.Rows[0]["Privilege"].ToString()) ? 0 : Convert.ToInt32(dt.Rows[0]["Privilege"].ToString());
                        iFaceIndex = string.IsNullOrEmpty(dt.Rows[0]["Face_Index"].ToString()) ? 0 : Convert.ToInt32(dt.Rows[0]["Face_Index"].ToString());
                        sTmpData = string.IsNullOrEmpty(dt.Rows[0]["Face_Image"].ToString()) ? " " : dt.Rows[0]["Face_Image"].ToString();
                        iLength = string.IsNullOrEmpty(dt.Rows[0]["Face_Length"].ToString()) ? 0 : Convert.ToInt32(dt.Rows[0]["Face_Length"].ToString());
                        sEnabled = string.IsNullOrEmpty(dt.Rows[0]["Enabled"].ToString()) ? " " : dt.Rows[0]["Enabled"].ToString();
                    }
                    if (sUserID != lastUserId)//identify whether the user information(except fingerprint templates) has been uploaded
                    {
                        if (axCZKEM1.SSR_SetUserInfo(iMachineNumber, sUserID, sName, sPassword, iPrivilege, Convert.ToBoolean(sEnabled)))//upload user information to the memory
                        {
                            axCZKEM1.SetUserFaceStr(iMachineNumber, sUserID, iFaceIndex, sTmpData, iLength);//upload templates information to the memory
                        }
                        else
                        {
                            axCZKEM1.GetLastError(ref idwErrorCode);
                            MessageBox.Show("Operation failed,ErrorCode=" + idwErrorCode.ToString(), "Error");
                            Cursor = Cursors.Default;
                            axCZKEM1.EnableDevice(iMachineNumber, true);
                            return;
                        }
                    }
                    else//the current fingerprint and the former one belongs the same user,that is ,one user has more than one template
                    {
                        axCZKEM1.SetUserFaceStr(iMachineNumber, sUserID, iFaceIndex, sTmpData, iLength);
                    }
                    sUserID = lastUserId;//change the value of userId dynamicly
                    MessageBox.Show("Successfully Upload the face templates, " + "total:" + lvFace.CheckedItems.Count.ToString(), "Success");
                }
                axCZKEM1.RefreshData(iMachineNumber);//the data in the device should be refreshed
                Cursor = Cursors.Default;
                axCZKEM1.EnableDevice(iMachineNumber, true);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
            finally
            {
                conn.Close();
            }
        }

        private void btnClearAdmin_Click(object sender, EventArgs e)
        {
            if (bIsConnected == false)
            {
                MessageBox.Show("Please connect the device first", "Error");
                return;
            }
            int idwErrorCode = 0;

            Cursor = Cursors.WaitCursor;
            if (axCZKEM1.ClearAdministrators(iMachineNumber))
            {
                axCZKEM1.RefreshData(iMachineNumber);//the data in the device should be refreshed
                MessageBox.Show("Successfully clear administrator privilege from teiminal!", "Success");


            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                MessageBox.Show("Operation failed,ErrorCode=" + idwErrorCode.ToString(), "Error");
            }
            Cursor = Cursors.Default;
           
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (cmbip.Text == "" || txtPort.Text == "")
            {
                MessageBox.Show("IP and Port cannot be null", "Error");
                return;
            }
            int idwErrorCode = 0;
            Cursor = Cursors.WaitCursor;
            if (btnConnect.Text == "DisConnect")
            {
                axCZKEM1.Disconnect();
                bIsConnected = false;
                btnConnect.Text = "Connect";
                axCZKEM1.RegEvent(iMachineNumber, 65535);
                lblstate.Text = "Current Status is disconnected";
                lblstate.ForeColor = Color.Red;
                Cursor = Cursors.Default;
                return;
            }
            //axCZKEM1.PullMode = 1;
            bIsConnected = axCZKEM1.Connect_Net(cmbip.Text, Convert.ToInt32(txtPort.Text));
            if (bIsConnected == true)
            {
                btnConnect.Text = "DisConnect";
                btnConnect.Refresh();
                lblstate.Text = "Current State is:Connected";
                lblstate.ForeColor = Color.Green;
                iMachineNumber = 1;
                axCZKEM1.RegEvent(iMachineNumber, 65535);
                MyCountFace = 2;
            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");
            }
            Cursor = Cursors.Default;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (bIsConnected == false)
            {
                MessageBox.Show("Please connect the device first!", "Error");
                return;
            }

            string sName = "", sPassword = "", sTmpData = "", sdwEnrollNumber = "";
            int iPrivilege = 0, idwFingerIndex = 0, iTmpLength = 0, iFlag = 0;
            bool bEnabled = false;
            lvDownload.Items.Clear();
            lvDownload.BeginUpdate();
            axCZKEM1.EnableDevice(iMachineNumber, false);
            Cursor = Cursors.WaitCursor;

            axCZKEM1.ReadAllUserID(iMachineNumber);//read all the user information to the memory
            axCZKEM1.ReadAllTemplate(iMachineNumber);//read all the users' fingerprint templates to the memory

            while (axCZKEM1.SSR_GetAllUserInfo(iMachineNumber, out sdwEnrollNumber, out sName, out sPassword, out iPrivilege, out bEnabled))//get all the users' information from the memory
            {
                for (idwFingerIndex = 0; idwFingerIndex < 10; idwFingerIndex++)
                {
                    if (axCZKEM1.GetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, out iFlag, out sTmpData, out iTmpLength))//get the corresponding templates string and length from the memory
                    {
                        ListViewItem list = new ListViewItem();
                        list.Text = sdwEnrollNumber.ToString();
                        list.SubItems.Add(sName);
                        list.SubItems.Add(idwFingerIndex.ToString());
                        list.SubItems.Add(sTmpData);
                        list.SubItems.Add(iPrivilege.ToString());
                        list.SubItems.Add(sPassword);
                        if (bEnabled == true)
                        {
                            list.SubItems.Add("true");
                        }
                        else
                        {
                            list.SubItems.Add("false");
                        }
                        list.SubItems.Add(iFlag.ToString());
                        lvDownload.Items.Add(list);

                        // save in Sqlserver database   
                        conn.Open();
                        string val = "select * from IFACEDevice_FingerTm where User_Id='" + sdwEnrollNumber + "' and Finger_Index='" + idwFingerIndex + "'";
                        SqlCommand valcmd = new SqlCommand(val, conn);
                        SqlDataAdapter da = new SqlDataAdapter(valcmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        if (dt.Rows.Count != 0)
                        {
                            conn.Close();
                            conn.Open();
                            string querydel = "delete from IFACEDevice_FingerTm where User_Id='" + sdwEnrollNumber + "'and Finger_Index='" + idwFingerIndex + "'";
                            SqlCommand cmddel = new SqlCommand(querydel, conn);
                            cmddel.ExecuteNonQuery();
                        }
                        conn.Close();
                        conn.Open();
                        string query = "INSERT INTO [IFACEDevice_FingerTm](User_Id,Name,Finger_Index,Finger_Image,Privilege,Passwords,Enabled,Flag) VALUES(@userId,@name,@fingerIndex,@fingerImage,@privilege,@password,@enabled,@flag)";
                        SqlCommand cmd = new SqlCommand(query, conn);
                        cmd.Parameters.AddWithValue("@userId", sdwEnrollNumber);
                        cmd.Parameters.AddWithValue("@name", sName);
                        cmd.Parameters.AddWithValue("@fingerIndex", idwFingerIndex);
                        cmd.Parameters.AddWithValue("@fingerImage", sTmpData);
                        cmd.Parameters.AddWithValue("@privilege", iPrivilege);
                        cmd.Parameters.AddWithValue("@password", sPassword);
                        cmd.Parameters.AddWithValue("@enabled", bEnabled);
                        cmd.Parameters.AddWithValue("@flag", iFlag);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
                // MessageBox.Show("Records inserted successfully");
            }

            lvDownload.EndUpdate();
            axCZKEM1.EnableDevice(iMachineNumber, true);
            Cursor = Cursors.Default;
            MessageBox.Show("Completed");
        }

        private void btnBatchUpdate_Click(object sender, EventArgs e)
        {
            if (bIsConnected == false)
            {
                MessageBox.Show("Please connect the device first!", "Error");
                return;
            }

            int idwErrorCode = 0;
            string sdwEnrollNumber = "";
            string sName = "";
            int idwFingerIndex = 0;
            string sTmpData = "";
            int iPrivilege = 0;
            string sPassword = "";
            string sEnabled = "";
            string sLastEnrollNumber = "";

            Cursor = Cursors.WaitCursor;
            axCZKEM1.EnableDevice(iMachineNumber, false);

            // select data from database
            DataTable dt = new DataTable();
            conn.Open();
            string upload_data = "SELECT * FROM  IFACEDevice_FingerTm";
            SqlCommand cmd = new SqlCommand(upload_data, conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            // start select data from database to upload in listview
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                sdwEnrollNumber = string.IsNullOrEmpty(dt.Rows[i]["User_Id"].ToString()) ? " " : dt.Rows[i]["User_Id"].ToString();
                sName = string.IsNullOrEmpty(dt.Rows[i]["Name"].ToString()) ? " " : dt.Rows[i]["Name"].ToString();
                idwFingerIndex = string.IsNullOrEmpty(dt.Rows[i]["Finger_Index"].ToString()) ? 0 : Convert.ToInt32(dt.Rows[i]["Finger_Index"].ToString());
                sTmpData = string.IsNullOrEmpty(dt.Rows[i]["Finger_Image"].ToString()) ? " " : dt.Rows[i]["Finger_Image"].ToString();
                iPrivilege = string.IsNullOrEmpty(dt.Rows[i]["Privilege"].ToString()) ? 0 : Convert.ToInt32(dt.Rows[i]["Privilege"].ToString());
                sPassword = string.IsNullOrEmpty(dt.Rows[i]["Passwords"].ToString()) ? null : dt.Rows[i]["Passwords"].ToString();
                sEnabled = string.IsNullOrEmpty(dt.Rows[i]["Enabled"].ToString()) ? " " : dt.Rows[i]["Enabled"].ToString();
                int iFlag = Convert.ToInt32(dt.Rows[i]["Flag"].ToString());

                if (sdwEnrollNumber != sLastEnrollNumber)//identify whether the user information(except fingerprint templates) has been uploaded
                {
                    if (axCZKEM1.SSR_SetUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, Convert.ToBoolean(sEnabled)))//upload user information to the memory
                    {
                        axCZKEM1.SetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, iFlag, sTmpData);//upload templates information to the memory
                    }
                    else
                    {
                        axCZKEM1.GetLastError(ref idwErrorCode);
                        MessageBox.Show("Operation failed,ErrorCode=" + idwErrorCode.ToString(), "Error");
                        Cursor = Cursors.Default;
                        axCZKEM1.EnableDevice(iMachineNumber, true);
                        return;
                    }
                }
                else//the current fingerprint and the former one belongs the same user,that is ,one user has more than one template
                {
                    axCZKEM1.SetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, iFlag, sTmpData);
                }
                sLastEnrollNumber = sdwEnrollNumber;//change the value of iLastEnrollNumber dynamicly
            }
            //end
            axCZKEM1.RefreshData(iMachineNumber);//the data in the device should be refreshed
            Cursor = Cursors.Default;
            axCZKEM1.EnableDevice(iMachineNumber, true);
            MessageBox.Show("Successfully upload fingerprint templates , " + "total:" + dt.Rows.Count.ToString(), "Success");
        }

        private void chck_CheckedChanged(object sender, EventArgs e)
        {

            if (chck.Checked == false)
            {
                foreach (ListViewItem list in lvFace.Items)
                {
                    list.Checked = false;
                }
            }
            else
            {
                foreach (ListViewItem list in lvFace.Items)
                {
                    list.Checked = true;
                }
            }
        }


    }
}
