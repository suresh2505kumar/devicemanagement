﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;


namespace DeviceManagement
{
    public partial class FrmChangePass : Form
    {
        public FrmChangePass()
        {
            InitializeComponent();
            txtpass.KeyUp += textBox_Compare;
            txtConpas.KeyUp += textBox_Compare;
        }
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        private void FrmChangePass_Load(object sender, EventArgs e)
        {
            txtUsername.Enabled = false;
            conn.Open();
            string Query = "Select UserName from Useradmin where Uid='" + GlobalVar.sUserId + "'";
            SqlCommand cmd = new SqlCommand(Query, conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count == 1)
            {
                txtUsername.Text = dt.Rows[0]["UserName"].ToString();                
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void textBox_Compare(object sender, KeyEventArgs e)
        {
            Color cBackColor = Color.Red;
            if (txtpass.Text == txtConpas.Text)
            {
                cBackColor = Color.Green;
            }
            txtpass.ForeColor = cBackColor;
            txtConpas.ForeColor = cBackColor;
        }

        private void Btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Close();
                conn.Open();
                if (txtCurrentPass.Text == "")
                {
                    MessageBox.Show("Enter Cureent Password");
                    return;
                }
                else if (txtConpas.Text == "" || txtpass.Text == "")
                {
                    MessageBox.Show("Enter New Password");
                    return;
                }
                else
                {
                    string vqlquery = "Select * from Useradmin where Username='" + txtUsername.Text + "' and Password='" + txtCurrentPass.Text + "'";
                    SqlCommand cmd = new SqlCommand(vqlquery, conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    if (dt.Rows.Count == 1)
                    {
                        conn.Close();
                        conn.Open();
                        string Query = "update useradmin set password='" + txtConpas.Text + "' where Uid=" + GlobalVar.sUserId + "";
                        SqlCommand cmdupdate = new SqlCommand(Query, conn);
                        cmdupdate.ExecuteNonQuery();
                        MessageBox.Show("Password Has been Changed Sucessfully");
                        cleartextbox();
                    }
                    else
                    {
                        MessageBox.Show("Entered Current Password is worng");
                        txtCurrentPass.Text = string.Empty;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
            }
            finally
            {
                conn.Close();
            }
        }
        protected void cleartextbox()
        {
            txtConpas.Text = string.Empty;
            txtCurrentPass.Text = string.Empty;
            txtpass.Text = string.Empty;
        }
    }
}
