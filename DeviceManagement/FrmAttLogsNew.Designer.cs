﻿namespace DeviceManagement
{
    partial class FrmAttLogsNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnDownloadLogs = new System.Windows.Forms.Button();
            this.GrAttLogs = new System.Windows.Forms.GroupBox();
            this.chckAutoClear = new System.Windows.Forms.CheckBox();
            this.DataGridAttLogs = new System.Windows.Forms.DataGridView();
            this.backWorkerAttlogs = new System.ComponentModel.BackgroundWorker();
            this.BtnDownloadToUSB = new System.Windows.Forms.Button();
            this.GrAttLogs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridAttLogs)).BeginInit();
            this.SuspendLayout();
            // 
            // btnConnect
            // 
            this.btnConnect.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnect.Location = new System.Drawing.Point(606, 16);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(127, 30);
            this.btnConnect.TabIndex = 0;
            this.btnConnect.Text = "Test Connection";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.BtnConnect_Click);
            // 
            // btnDownloadLogs
            // 
            this.btnDownloadLogs.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDownloadLogs.Location = new System.Drawing.Point(739, 17);
            this.btnDownloadLogs.Name = "btnDownloadLogs";
            this.btnDownloadLogs.Size = new System.Drawing.Size(133, 28);
            this.btnDownloadLogs.TabIndex = 1;
            this.btnDownloadLogs.Text = "Download AttLogs";
            this.btnDownloadLogs.UseVisualStyleBackColor = true;
            this.btnDownloadLogs.Click += new System.EventHandler(this.BtnDownloadLogs_Click);
            // 
            // GrAttLogs
            // 
            this.GrAttLogs.Controls.Add(this.BtnDownloadToUSB);
            this.GrAttLogs.Controls.Add(this.chckAutoClear);
            this.GrAttLogs.Controls.Add(this.DataGridAttLogs);
            this.GrAttLogs.Controls.Add(this.btnConnect);
            this.GrAttLogs.Controls.Add(this.btnDownloadLogs);
            this.GrAttLogs.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrAttLogs.Location = new System.Drawing.Point(8, 1);
            this.GrAttLogs.Name = "GrAttLogs";
            this.GrAttLogs.Size = new System.Drawing.Size(881, 486);
            this.GrAttLogs.TabIndex = 5;
            this.GrAttLogs.TabStop = false;
            // 
            // chckAutoClear
            // 
            this.chckAutoClear.AutoSize = true;
            this.chckAutoClear.Location = new System.Drawing.Point(6, 23);
            this.chckAutoClear.Name = "chckAutoClear";
            this.chckAutoClear.Size = new System.Drawing.Size(92, 22);
            this.chckAutoClear.TabIndex = 7;
            this.chckAutoClear.Text = "Auto Clear";
            this.chckAutoClear.UseVisualStyleBackColor = true;
            // 
            // DataGridAttLogs
            // 
            this.DataGridAttLogs.AllowUserToAddRows = false;
            this.DataGridAttLogs.BackgroundColor = System.Drawing.Color.White;
            this.DataGridAttLogs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridAttLogs.Location = new System.Drawing.Point(6, 50);
            this.DataGridAttLogs.Name = "DataGridAttLogs";
            this.DataGridAttLogs.RowHeadersVisible = false;
            this.DataGridAttLogs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridAttLogs.Size = new System.Drawing.Size(866, 430);
            this.DataGridAttLogs.TabIndex = 5;
            this.DataGridAttLogs.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridAttLogs_CellDoubleClick);
            // 
            // BtnDownloadToUSB
            // 
            this.BtnDownloadToUSB.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownloadToUSB.Location = new System.Drawing.Point(401, 16);
            this.BtnDownloadToUSB.Name = "BtnDownloadToUSB";
            this.BtnDownloadToUSB.Size = new System.Drawing.Size(163, 28);
            this.BtnDownloadToUSB.TabIndex = 8;
            this.BtnDownloadToUSB.Text = "Download From USB";
            this.BtnDownloadToUSB.UseVisualStyleBackColor = true;
            this.BtnDownloadToUSB.Click += new System.EventHandler(this.BtnDownloadToUSB_Click);
            // 
            // FrmAttLogsNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(892, 498);
            this.Controls.Add(this.GrAttLogs);
            this.Name = "FrmAttLogsNew";
            this.Text = "Attendance Logs";
            this.Load += new System.EventHandler(this.FrmAttLogsNew_Load);
            this.GrAttLogs.ResumeLayout(false);
            this.GrAttLogs.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridAttLogs)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnDownloadLogs;
        private System.Windows.Forms.GroupBox GrAttLogs;
        private System.Windows.Forms.DataGridView DataGridAttLogs;
        private System.ComponentModel.BackgroundWorker backWorkerAttlogs;
        private System.Windows.Forms.CheckBox chckAutoClear;
        private System.Windows.Forms.Button BtnDownloadToUSB;
    }
}