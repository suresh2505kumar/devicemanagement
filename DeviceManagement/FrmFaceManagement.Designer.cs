﻿namespace DeviceManagement
{
    partial class FrmFaceManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDelete = new System.Windows.Forms.Button();
            this.chck = new System.Windows.Forms.CheckBox();
            this.btnUpldUsers = new System.Windows.Forms.Button();
            this.btnClearAdmin = new System.Windows.Forms.Button();
            this.btnDownLoadFace = new System.Windows.Forms.Button();
            this.btnUploadFace = new System.Windows.Forms.Button();
            this.lvFace = new System.Windows.Forms.ListView();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPageFaceTm = new System.Windows.Forms.TabPage();
            this.tabBody = new System.Windows.Forms.TabControl();
            this.tabPageFT = new System.Windows.Forms.TabPage();
            this.lvDownload = new System.Windows.Forms.ListView();
            this.ch1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ch2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ch3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ch4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ch5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ch6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ch7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ch8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnBatchUpdate = new System.Windows.Forms.Button();
            this.grattlogs = new System.Windows.Forms.GroupBox();
            this.txtKey = new System.Windows.Forms.TextBox();
            this.lblstate = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnConnect = new System.Windows.Forms.Button();
            this.cmbip = new System.Windows.Forms.ComboBox();
            this.pgbarStatus = new System.Windows.Forms.ProgressBar();
            this.tabPageFaceTm.SuspendLayout();
            this.tabBody.SuspendLayout();
            this.tabPageFT.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.grattlogs.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(367, 41);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(153, 23);
            this.btnDelete.TabIndex = 101;
            this.btnDelete.Text = "Delete Users";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Visible = false;
            // 
            // chck
            // 
            this.chck.AutoSize = true;
            this.chck.Location = new System.Drawing.Point(8, 336);
            this.chck.Name = "chck";
            this.chck.Size = new System.Drawing.Size(70, 17);
            this.chck.TabIndex = 99;
            this.chck.Text = "Select All";
            this.chck.UseVisualStyleBackColor = true;
            this.chck.CheckedChanged += new System.EventHandler(this.chck_CheckedChanged);
            // 
            // btnUpldUsers
            // 
            this.btnUpldUsers.ForeColor = System.Drawing.Color.Black;
            this.btnUpldUsers.Location = new System.Drawing.Point(284, 353);
            this.btnUpldUsers.Name = "btnUpldUsers";
            this.btnUpldUsers.Size = new System.Drawing.Size(136, 23);
            this.btnUpldUsers.TabIndex = 97;
            this.btnUpldUsers.Text = "Upload Selected users";
            this.btnUpldUsers.UseVisualStyleBackColor = true;
            this.btnUpldUsers.Click += new System.EventHandler(this.btnUpldUsers_Click);
            // 
            // btnClearAdmin
            // 
            this.btnClearAdmin.ForeColor = System.Drawing.Color.Black;
            this.btnClearAdmin.Location = new System.Drawing.Point(422, 353);
            this.btnClearAdmin.Name = "btnClearAdmin";
            this.btnClearAdmin.Size = new System.Drawing.Size(75, 23);
            this.btnClearAdmin.TabIndex = 96;
            this.btnClearAdmin.Text = "Clear Administrators";
            this.btnClearAdmin.UseVisualStyleBackColor = true;
            this.btnClearAdmin.Click += new System.EventHandler(this.btnClearAdmin_Click);
            // 
            // btnDownLoadFace
            // 
            this.btnDownLoadFace.Location = new System.Drawing.Point(8, 353);
            this.btnDownLoadFace.Name = "btnDownLoadFace";
            this.btnDownLoadFace.Size = new System.Drawing.Size(141, 23);
            this.btnDownLoadFace.TabIndex = 70;
            this.btnDownLoadFace.Text = "DownLoadFace to DB";
            this.btnDownLoadFace.UseVisualStyleBackColor = true;
            this.btnDownLoadFace.Click += new System.EventHandler(this.btnDownLoadFace_Click);
            // 
            // btnUploadFace
            // 
            this.btnUploadFace.Location = new System.Drawing.Point(159, 353);
            this.btnUploadFace.Name = "btnUploadFace";
            this.btnUploadFace.Size = new System.Drawing.Size(117, 23);
            this.btnUploadFace.TabIndex = 69;
            this.btnUploadFace.Text = "UploadFace From DB";
            this.btnUploadFace.UseVisualStyleBackColor = true;
            this.btnUploadFace.Click += new System.EventHandler(this.btnUploadFace_Click);
            // 
            // lvFace
            // 
            this.lvFace.CheckBoxes = true;
            this.lvFace.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14});
            this.lvFace.FullRowSelect = true;
            this.lvFace.GridLines = true;
            this.lvFace.Location = new System.Drawing.Point(8, 6);
            this.lvFace.Name = "lvFace";
            this.lvFace.Size = new System.Drawing.Size(480, 330);
            this.lvFace.TabIndex = 68;
            this.lvFace.UseCompatibleStateImageBehavior = false;
            this.lvFace.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "UserID";
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Name";
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Password";
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Privilege";
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "FaceIndex";
            this.columnHeader11.Width = 42;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "TmpData";
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Length";
            this.columnHeader13.Width = 40;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Enabled";
            // 
            // tabPageFaceTm
            // 
            this.tabPageFaceTm.Controls.Add(this.lvFace);
            this.tabPageFaceTm.Controls.Add(this.chck);
            this.tabPageFaceTm.Controls.Add(this.btnUpldUsers);
            this.tabPageFaceTm.Controls.Add(this.btnClearAdmin);
            this.tabPageFaceTm.Controls.Add(this.btnDownLoadFace);
            this.tabPageFaceTm.Controls.Add(this.btnUploadFace);
            this.tabPageFaceTm.Location = new System.Drawing.Point(4, 25);
            this.tabPageFaceTm.Name = "tabPageFaceTm";
            this.tabPageFaceTm.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageFaceTm.Size = new System.Drawing.Size(499, 391);
            this.tabPageFaceTm.TabIndex = 1;
            this.tabPageFaceTm.Text = "Face Tmps";
            this.tabPageFaceTm.UseVisualStyleBackColor = true;
            // 
            // tabBody
            // 
            this.tabBody.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabBody.Controls.Add(this.tabPageFaceTm);
            this.tabBody.Controls.Add(this.tabPageFT);
            this.tabBody.Location = new System.Drawing.Point(5, 73);
            this.tabBody.Name = "tabBody";
            this.tabBody.SelectedIndex = 0;
            this.tabBody.Size = new System.Drawing.Size(507, 420);
            this.tabBody.TabIndex = 100;
            // 
            // tabPageFT
            // 
            this.tabPageFT.Controls.Add(this.lvDownload);
            this.tabPageFT.Controls.Add(this.groupBox6);
            this.tabPageFT.Location = new System.Drawing.Point(4, 25);
            this.tabPageFT.Name = "tabPageFT";
            this.tabPageFT.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageFT.Size = new System.Drawing.Size(499, 391);
            this.tabPageFT.TabIndex = 0;
            this.tabPageFT.Text = "Fingerprint Tmps";
            this.tabPageFT.UseVisualStyleBackColor = true;
            // 
            // lvDownload
            // 
            this.lvDownload.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ch1,
            this.ch2,
            this.ch3,
            this.ch4,
            this.ch5,
            this.ch6,
            this.ch7,
            this.ch8});
            this.lvDownload.GridLines = true;
            this.lvDownload.Location = new System.Drawing.Point(9, 31);
            this.lvDownload.Name = "lvDownload";
            this.lvDownload.Size = new System.Drawing.Size(479, 268);
            this.lvDownload.TabIndex = 10;
            this.lvDownload.UseCompatibleStateImageBehavior = false;
            this.lvDownload.View = System.Windows.Forms.View.Details;
            // 
            // ch1
            // 
            this.ch1.Text = "UserID";
            this.ch1.Width = 54;
            // 
            // ch2
            // 
            this.ch2.Text = "Name";
            this.ch2.Width = 41;
            // 
            // ch3
            // 
            this.ch3.Text = "FingerIndex";
            this.ch3.Width = 52;
            // 
            // ch4
            // 
            this.ch4.Text = "tmpData";
            this.ch4.Width = 61;
            // 
            // ch5
            // 
            this.ch5.Text = "Privilege";
            this.ch5.Width = 77;
            // 
            // ch6
            // 
            this.ch6.Text = "Password";
            this.ch6.Width = 40;
            // 
            // ch7
            // 
            this.ch7.Text = "Ennabled";
            this.ch7.Width = 68;
            // 
            // ch8
            // 
            this.ch8.Text = "Flag";
            this.ch8.Width = 40;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.button1);
            this.groupBox6.Controls.Add(this.btnBatchUpdate);
            this.groupBox6.Location = new System.Drawing.Point(8, 306);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(479, 67);
            this.groupBox6.TabIndex = 11;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Fingerprint Templates of 9.0&&10.0 Arithmetic";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(33, 23);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(135, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Download FP To DB";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnBatchUpdate
            // 
            this.btnBatchUpdate.Location = new System.Drawing.Point(228, 23);
            this.btnBatchUpdate.Name = "btnBatchUpdate";
            this.btnBatchUpdate.Size = new System.Drawing.Size(134, 23);
            this.btnBatchUpdate.TabIndex = 5;
            this.btnBatchUpdate.Text = "UploadFingerPrint";
            this.btnBatchUpdate.UseVisualStyleBackColor = true;
            this.btnBatchUpdate.Click += new System.EventHandler(this.btnBatchUpdate_Click);
            // 
            // grattlogs
            // 
            this.grattlogs.BackColor = System.Drawing.SystemColors.Control;
            this.grattlogs.Controls.Add(this.txtKey);
            this.grattlogs.Controls.Add(this.lblstate);
            this.grattlogs.Controls.Add(this.label2);
            this.grattlogs.Controls.Add(this.txtPort);
            this.grattlogs.Controls.Add(this.cmbip);
            this.grattlogs.Controls.Add(this.label1);
            this.grattlogs.Location = new System.Drawing.Point(5, 4);
            this.grattlogs.Name = "grattlogs";
            this.grattlogs.Size = new System.Drawing.Size(356, 63);
            this.grattlogs.TabIndex = 98;
            this.grattlogs.TabStop = false;
            // 
            // txtKey
            // 
            this.txtKey.Enabled = false;
            this.txtKey.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKey.Location = new System.Drawing.Point(222, 15);
            this.txtKey.Name = "txtKey";
            this.txtKey.Size = new System.Drawing.Size(30, 22);
            this.txtKey.TabIndex = 5;
            this.txtKey.Visible = false;
            // 
            // lblstate
            // 
            this.lblstate.Location = new System.Drawing.Point(19, 37);
            this.lblstate.Name = "lblstate";
            this.lblstate.Size = new System.Drawing.Size(320, 19);
            this.lblstate.TabIndex = 4;
            this.lblstate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(258, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Port";
            // 
            // txtPort
            // 
            this.txtPort.Enabled = false;
            this.txtPort.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPort.Location = new System.Drawing.Point(309, 15);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(30, 22);
            this.txtPort.TabIndex = 2;
            this.txtPort.Text = "4370";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ip address";
            // 
            // btnConnect
            // 
            this.btnConnect.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnect.Location = new System.Drawing.Point(367, 4);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(153, 23);
            this.btnConnect.TabIndex = 99;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // cmbip
            // 
            this.cmbip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbip.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbip.FormattingEnabled = true;
            this.cmbip.Location = new System.Drawing.Point(92, 13);
            this.cmbip.Name = "cmbip";
            this.cmbip.Size = new System.Drawing.Size(121, 23);
            this.cmbip.TabIndex = 1;
            // 
            // pgbarStatus
            // 
            this.pgbarStatus.Location = new System.Drawing.Point(50, 493);
            this.pgbarStatus.Name = "pgbarStatus";
            this.pgbarStatus.Size = new System.Drawing.Size(428, 23);
            this.pgbarStatus.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pgbarStatus.TabIndex = 102;
            // 
            // FrmFaceManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 495);
            this.Controls.Add(this.pgbarStatus);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.tabBody);
            this.Controls.Add(this.grattlogs);
            this.Controls.Add(this.btnConnect);
            this.Name = "FrmFaceManagement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Face User Info";
            this.Load += new System.EventHandler(this.FrmFaceManagement_Load);
            this.tabPageFaceTm.ResumeLayout(false);
            this.tabPageFaceTm.PerformLayout();
            this.tabBody.ResumeLayout(false);
            this.tabPageFT.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.grattlogs.ResumeLayout(false);
            this.grattlogs.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.CheckBox chck;
        private System.Windows.Forms.Button btnUpldUsers;
        private System.Windows.Forms.Button btnClearAdmin;
        private System.Windows.Forms.Button btnDownLoadFace;
        private System.Windows.Forms.Button btnUploadFace;
        private System.Windows.Forms.ListView lvFace;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.TabPage tabPageFaceTm;
        private System.Windows.Forms.TabControl tabBody;
        private System.Windows.Forms.TabPage tabPageFT;
        private System.Windows.Forms.ListView lvDownload;
        private System.Windows.Forms.ColumnHeader ch1;
        private System.Windows.Forms.ColumnHeader ch2;
        private System.Windows.Forms.ColumnHeader ch3;
        private System.Windows.Forms.ColumnHeader ch4;
        private System.Windows.Forms.ColumnHeader ch5;
        private System.Windows.Forms.ColumnHeader ch6;
        private System.Windows.Forms.ColumnHeader ch7;
        private System.Windows.Forms.ColumnHeader ch8;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnBatchUpdate;
        private System.Windows.Forms.GroupBox grattlogs;
        private System.Windows.Forms.TextBox txtKey;
        private System.Windows.Forms.Label lblstate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.ComboBox cmbip;
        private System.Windows.Forms.ProgressBar pgbarStatus;
    }
}