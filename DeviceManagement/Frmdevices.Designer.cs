﻿namespace DeviceManagement
{
    partial class Frmdevices
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDeviceManage = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lnklblSerNo = new System.Windows.Forms.LinkLabel();
            this.cmbDirection = new System.Windows.Forms.ComboBox();
            this.TxtDeviceID = new System.Windows.Forms.TextBox();
            this.TxtSerialNo = new System.Windows.Forms.TextBox();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.TxtIp = new System.Windows.Forms.TextBox();
            this.txtKey = new System.Windows.Forms.TextBox();
            this.BtnExit = new System.Windows.Forms.Button();
            this.cmbDeviceType = new System.Windows.Forms.ComboBox();
            this.BtnEdit = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.BtnSave = new System.Windows.Forms.Button();
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.btnDecrypt = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtDeviceName = new System.Windows.Forms.TextBox();
            this.DataGRDevices = new System.Windows.Forms.DataGridView();
            this.grBack = new System.Windows.Forms.GroupBox();
            this.GrFront = new System.Windows.Forms.GroupBox();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnRestartDevice = new System.Windows.Forms.Button();
            this.btnChangeIp = new System.Windows.Forms.Button();
            this.btnClearAdmin = new System.Windows.Forms.Button();
            this.btnRecordsCount = new System.Windows.Forms.Button();
            this.btndeviceInfo = new System.Windows.Forms.Button();
            this.btnClerLogs = new System.Windows.Forms.Button();
            this.txtCurrent = new System.Windows.Forms.TextBox();
            this.btnSetDatetime = new System.Windows.Forms.Button();
            this.txtDeviceDatetime = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.btnTestConnection = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbIp = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.DataGRDevices)).BeginInit();
            this.grBack.SuspendLayout();
            this.GrFront.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDeviceManage
            // 
            this.btnDeviceManage.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeviceManage.Location = new System.Drawing.Point(14, 509);
            this.btnDeviceManage.Name = "btnDeviceManage";
            this.btnDeviceManage.Size = new System.Drawing.Size(150, 28);
            this.btnDeviceManage.TabIndex = 22;
            this.btnDeviceManage.Text = "Manage Device";
            this.btnDeviceManage.UseVisualStyleBackColor = true;
            this.btnDeviceManage.Click += new System.EventHandler(this.btnDeviceManage_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(132, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 18);
            this.label3.TabIndex = 12;
            this.label3.Text = "Device Direction";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(168, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 18);
            this.label2.TabIndex = 11;
            this.label2.Text = "Ip Address";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(99, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 18);
            this.label4.TabIndex = 13;
            this.label4.Text = "Device Serial Number";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(151, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 18);
            this.label1.TabIndex = 10;
            this.label1.Text = "Device Name";
            // 
            // lnklblSerNo
            // 
            this.lnklblSerNo.AutoSize = true;
            this.lnklblSerNo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnklblSerNo.Location = new System.Drawing.Point(452, 104);
            this.lnklblSerNo.Name = "lnklblSerNo";
            this.lnklblSerNo.Size = new System.Drawing.Size(15, 18);
            this.lnklblSerNo.TabIndex = 4;
            this.lnklblSerNo.TabStop = true;
            this.lnklblSerNo.Text = "?";
            this.lnklblSerNo.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblSerNo_LinkClicked);
            // 
            // cmbDirection
            // 
            this.cmbDirection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDirection.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDirection.FormattingEnabled = true;
            this.cmbDirection.Items.AddRange(new object[] {
            "In Device",
            "Out Device",
            "Altenate In/Out"});
            this.cmbDirection.Location = new System.Drawing.Point(247, 72);
            this.cmbDirection.Name = "cmbDirection";
            this.cmbDirection.Size = new System.Drawing.Size(200, 26);
            this.cmbDirection.TabIndex = 2;
            // 
            // TxtDeviceID
            // 
            this.TxtDeviceID.Location = new System.Drawing.Point(6, 27);
            this.TxtDeviceID.Name = "TxtDeviceID";
            this.TxtDeviceID.Size = new System.Drawing.Size(33, 20);
            this.TxtDeviceID.TabIndex = 14;
            this.TxtDeviceID.Visible = false;
            // 
            // TxtSerialNo
            // 
            this.TxtSerialNo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSerialNo.Location = new System.Drawing.Point(247, 103);
            this.TxtSerialNo.Name = "TxtSerialNo";
            this.TxtSerialNo.Size = new System.Drawing.Size(200, 26);
            this.TxtSerialNo.TabIndex = 3;
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(6, 58);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(33, 20);
            this.txtPort.TabIndex = 15;
            this.txtPort.Text = "4370";
            this.txtPort.Visible = false;
            // 
            // TxtIp
            // 
            this.TxtIp.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIp.Location = new System.Drawing.Point(247, 42);
            this.TxtIp.Name = "TxtIp";
            this.TxtIp.Size = new System.Drawing.Size(200, 26);
            this.TxtIp.TabIndex = 1;
            // 
            // txtKey
            // 
            this.txtKey.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKey.Location = new System.Drawing.Point(6, 80);
            this.txtKey.Name = "txtKey";
            this.txtKey.Size = new System.Drawing.Size(61, 22);
            this.txtKey.TabIndex = 16;
            this.txtKey.Visible = false;
            // 
            // BtnExit
            // 
            this.BtnExit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExit.Location = new System.Drawing.Point(493, 79);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(75, 30);
            this.BtnExit.TabIndex = 8;
            this.BtnExit.Text = "Close";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // cmbDeviceType
            // 
            this.cmbDeviceType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDeviceType.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDeviceType.FormattingEnabled = true;
            this.cmbDeviceType.Items.AddRange(new object[] {
            "BW Device",
            "TFT Device",
            "IFACE Device"});
            this.cmbDeviceType.Location = new System.Drawing.Point(247, 164);
            this.cmbDeviceType.Name = "cmbDeviceType";
            this.cmbDeviceType.Size = new System.Drawing.Size(200, 26);
            this.cmbDeviceType.TabIndex = 5;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Location = new System.Drawing.Point(493, 46);
            this.BtnEdit.Name = "BtnEdit";
            this.BtnEdit.Size = new System.Drawing.Size(75, 27);
            this.BtnEdit.TabIndex = 7;
            this.BtnEdit.Text = "Edit";
            this.BtnEdit.UseVisualStyleBackColor = true;
            this.BtnEdit.Click += new System.EventHandler(this.BtnEdit_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(159, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 18);
            this.label5.TabIndex = 19;
            this.label5.Text = "Device Type";
            // 
            // BtnSave
            // 
            this.BtnSave.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Location = new System.Drawing.Point(493, 9);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 28);
            this.BtnSave.TabIndex = 6;
            this.BtnSave.Text = "Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // txtLocation
            // 
            this.txtLocation.Enabled = false;
            this.txtLocation.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.Location = new System.Drawing.Point(247, 133);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Size = new System.Drawing.Size(200, 26);
            this.txtLocation.TabIndex = 4;
            // 
            // btnDecrypt
            // 
            this.btnDecrypt.Location = new System.Drawing.Point(6, 15);
            this.btnDecrypt.Name = "btnDecrypt";
            this.btnDecrypt.Size = new System.Drawing.Size(75, 23);
            this.btnDecrypt.TabIndex = 17;
            this.btnDecrypt.Text = "SeriyalNum";
            this.btnDecrypt.UseVisualStyleBackColor = true;
            this.btnDecrypt.Visible = false;
            this.btnDecrypt.Click += new System.EventHandler(this.btnDecrypt_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(137, 137);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 18);
            this.label6.TabIndex = 21;
            this.label6.Text = "Device Location";
            // 
            // TxtDeviceName
            // 
            this.TxtDeviceName.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeviceName.Location = new System.Drawing.Point(247, 11);
            this.TxtDeviceName.Name = "TxtDeviceName";
            this.TxtDeviceName.Size = new System.Drawing.Size(200, 26);
            this.TxtDeviceName.TabIndex = 0;
            // 
            // DataGRDevices
            // 
            this.DataGRDevices.AllowUserToAddRows = false;
            this.DataGRDevices.BackgroundColor = System.Drawing.Color.White;
            this.DataGRDevices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGRDevices.Location = new System.Drawing.Point(6, 194);
            this.DataGRDevices.Name = "DataGRDevices";
            this.DataGRDevices.ReadOnly = true;
            this.DataGRDevices.RowHeadersVisible = false;
            this.DataGRDevices.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGRDevices.Size = new System.Drawing.Size(657, 304);
            this.DataGRDevices.TabIndex = 22;
            this.DataGRDevices.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGRDevices_CellMouseDoubleClick);
            this.DataGRDevices.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGRDevices_KeyDown_1);
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.DataGRDevices);
            this.grBack.Controls.Add(this.TxtDeviceName);
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.btnDecrypt);
            this.grBack.Controls.Add(this.txtLocation);
            this.grBack.Controls.Add(this.BtnSave);
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.BtnEdit);
            this.grBack.Controls.Add(this.cmbDeviceType);
            this.grBack.Controls.Add(this.BtnExit);
            this.grBack.Controls.Add(this.txtKey);
            this.grBack.Controls.Add(this.TxtIp);
            this.grBack.Controls.Add(this.txtPort);
            this.grBack.Controls.Add(this.TxtSerialNo);
            this.grBack.Controls.Add(this.TxtDeviceID);
            this.grBack.Controls.Add(this.cmbDirection);
            this.grBack.Controls.Add(this.lnklblSerNo);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Location = new System.Drawing.Point(12, 3);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(671, 504);
            this.grBack.TabIndex = 23;
            this.grBack.TabStop = false;
            // 
            // GrFront
            // 
            this.GrFront.Controls.Add(this.btnBack);
            this.GrFront.Controls.Add(this.btnRestartDevice);
            this.GrFront.Controls.Add(this.btnChangeIp);
            this.GrFront.Controls.Add(this.btnClearAdmin);
            this.GrFront.Controls.Add(this.btnRecordsCount);
            this.GrFront.Controls.Add(this.btndeviceInfo);
            this.GrFront.Controls.Add(this.btnClerLogs);
            this.GrFront.Controls.Add(this.txtCurrent);
            this.GrFront.Controls.Add(this.btnSetDatetime);
            this.GrFront.Controls.Add(this.txtDeviceDatetime);
            this.GrFront.Controls.Add(this.button2);
            this.GrFront.Controls.Add(this.btnTestConnection);
            this.GrFront.Controls.Add(this.label7);
            this.GrFront.Controls.Add(this.cmbIp);
            this.GrFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrFront.Location = new System.Drawing.Point(12, 1);
            this.GrFront.Name = "GrFront";
            this.GrFront.Size = new System.Drawing.Size(671, 506);
            this.GrFront.TabIndex = 23;
            this.GrFront.TabStop = false;
            this.GrFront.Visible = false;
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(570, 469);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 31);
            this.btnBack.TabIndex = 35;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnRestartDevice
            // 
            this.btnRestartDevice.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRestartDevice.Location = new System.Drawing.Point(440, 207);
            this.btnRestartDevice.Name = "btnRestartDevice";
            this.btnRestartDevice.Size = new System.Drawing.Size(205, 28);
            this.btnRestartDevice.TabIndex = 34;
            this.btnRestartDevice.Text = "Restart Device";
            this.btnRestartDevice.UseVisualStyleBackColor = true;
            this.btnRestartDevice.Click += new System.EventHandler(this.btnRestartDevice_Click);
            // 
            // btnChangeIp
            // 
            this.btnChangeIp.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChangeIp.Location = new System.Drawing.Point(217, 207);
            this.btnChangeIp.Name = "btnChangeIp";
            this.btnChangeIp.Size = new System.Drawing.Size(204, 28);
            this.btnChangeIp.TabIndex = 33;
            this.btnChangeIp.Text = "Change Ip";
            this.btnChangeIp.UseVisualStyleBackColor = true;
            // 
            // btnClearAdmin
            // 
            this.btnClearAdmin.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearAdmin.Location = new System.Drawing.Point(31, 207);
            this.btnClearAdmin.Name = "btnClearAdmin";
            this.btnClearAdmin.Size = new System.Drawing.Size(159, 28);
            this.btnClearAdmin.TabIndex = 32;
            this.btnClearAdmin.Text = "Clear Admin Privileges";
            this.btnClearAdmin.UseVisualStyleBackColor = true;
            this.btnClearAdmin.Click += new System.EventHandler(this.btnClearAdmin_Click);
            // 
            // btnRecordsCount
            // 
            this.btnRecordsCount.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecordsCount.Location = new System.Drawing.Point(440, 135);
            this.btnRecordsCount.Name = "btnRecordsCount";
            this.btnRecordsCount.Size = new System.Drawing.Size(205, 28);
            this.btnRecordsCount.TabIndex = 31;
            this.btnRecordsCount.Text = "Get Records Count";
            this.btnRecordsCount.UseVisualStyleBackColor = true;
            this.btnRecordsCount.Click += new System.EventHandler(this.button5_Click);
            // 
            // btndeviceInfo
            // 
            this.btndeviceInfo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndeviceInfo.Location = new System.Drawing.Point(217, 135);
            this.btndeviceInfo.Name = "btndeviceInfo";
            this.btndeviceInfo.Size = new System.Drawing.Size(204, 28);
            this.btndeviceInfo.TabIndex = 30;
            this.btndeviceInfo.Text = "Get Device Info";
            this.btndeviceInfo.UseVisualStyleBackColor = true;
            this.btndeviceInfo.Click += new System.EventHandler(this.btndeviceInfo_Click);
            // 
            // btnClerLogs
            // 
            this.btnClerLogs.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClerLogs.Location = new System.Drawing.Point(31, 135);
            this.btnClerLogs.Name = "btnClerLogs";
            this.btnClerLogs.Size = new System.Drawing.Size(159, 28);
            this.btnClerLogs.TabIndex = 29;
            this.btnClerLogs.Text = "Clear Logs";
            this.btnClerLogs.UseVisualStyleBackColor = true;
            this.btnClerLogs.Click += new System.EventHandler(this.btnClerLogs_Click);
            // 
            // txtCurrent
            // 
            this.txtCurrent.Location = new System.Drawing.Point(440, 90);
            this.txtCurrent.Name = "txtCurrent";
            this.txtCurrent.Size = new System.Drawing.Size(205, 26);
            this.txtCurrent.TabIndex = 28;
            this.txtCurrent.Visible = false;
            // 
            // btnSetDatetime
            // 
            this.btnSetDatetime.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSetDatetime.Location = new System.Drawing.Point(440, 60);
            this.btnSetDatetime.Name = "btnSetDatetime";
            this.btnSetDatetime.Size = new System.Drawing.Size(205, 28);
            this.btnSetDatetime.TabIndex = 27;
            this.btnSetDatetime.Text = "Set Current Date and Time";
            this.btnSetDatetime.UseVisualStyleBackColor = true;
            // 
            // txtDeviceDatetime
            // 
            this.txtDeviceDatetime.Location = new System.Drawing.Point(217, 90);
            this.txtDeviceDatetime.Name = "txtDeviceDatetime";
            this.txtDeviceDatetime.Size = new System.Drawing.Size(204, 26);
            this.txtDeviceDatetime.TabIndex = 26;
            this.txtDeviceDatetime.Visible = false;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(217, 60);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(204, 28);
            this.button2.TabIndex = 25;
            this.button2.Text = "Get Device Date and Time";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // btnTestConnection
            // 
            this.btnTestConnection.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTestConnection.Location = new System.Drawing.Point(31, 60);
            this.btnTestConnection.Name = "btnTestConnection";
            this.btnTestConnection.Size = new System.Drawing.Size(159, 28);
            this.btnTestConnection.TabIndex = 24;
            this.btnTestConnection.Text = "Test Connection";
            this.btnTestConnection.UseVisualStyleBackColor = true;
            this.btnTestConnection.Click += new System.EventHandler(this.btnTestConnection_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(159, 18);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 18);
            this.label7.TabIndex = 1;
            this.label7.Text = "Ip Address";
            // 
            // cmbIp
            // 
            this.cmbIp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbIp.FormattingEnabled = true;
            this.cmbIp.Location = new System.Drawing.Point(238, 14);
            this.cmbIp.Name = "cmbIp";
            this.cmbIp.Size = new System.Drawing.Size(229, 26);
            this.cmbIp.TabIndex = 0;
            // 
            // Frmdevices
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(689, 539);
            this.Controls.Add(this.btnDeviceManage);
            this.Controls.Add(this.grBack);
            this.Controls.Add(this.GrFront);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frmdevices";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Devices";
            this.Load += new System.EventHandler(this.Frmdevices_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGRDevices)).EndInit();
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            this.GrFront.ResumeLayout(false);
            this.GrFront.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnDeviceManage;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel lnklblSerNo;
        private System.Windows.Forms.ComboBox cmbDirection;
        private System.Windows.Forms.TextBox TxtDeviceID;
        private System.Windows.Forms.TextBox TxtSerialNo;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.TextBox TxtIp;
        private System.Windows.Forms.TextBox txtKey;
        private System.Windows.Forms.Button BtnExit;
        private System.Windows.Forms.ComboBox cmbDeviceType;
        private System.Windows.Forms.Button BtnEdit;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.TextBox txtLocation;
        private System.Windows.Forms.Button btnDecrypt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtDeviceName;
        private System.Windows.Forms.DataGridView DataGRDevices;
        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.GroupBox GrFront;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbIp;
        private System.Windows.Forms.TextBox txtDeviceDatetime;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnTestConnection;
        private System.Windows.Forms.TextBox txtCurrent;
        private System.Windows.Forms.Button btnSetDatetime;
        private System.Windows.Forms.Button btnRecordsCount;
        private System.Windows.Forms.Button btndeviceInfo;
        private System.Windows.Forms.Button btnClerLogs;
        private System.Windows.Forms.Button btnRestartDevice;
        private System.Windows.Forms.Button btnChangeIp;
        private System.Windows.Forms.Button btnClearAdmin;
        private System.Windows.Forms.Button btnBack;
    }
}