﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Security.Cryptography;

namespace DeviceManagement
{
    public partial class FrmSerialGenerate : Form
    {
        public FrmSerialGenerate()
        {
            InitializeComponent();
        }
        string key, EnValue; 
        public SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        private void FrmSerialGenerate_Load(object sender, EventArgs e)
        {
            txtKey.Text = "o7x8y6";
            try
            {
                
                string query = "select DeviceID,DeviceSrNo from MyDevices order by DeviceID";
                SqlCommand cmd = new SqlCommand(query, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                lstSerialNo.DataSource = null;
                lstSerialNo.DataSource = dt;
                lstSerialNo.DisplayMember = "DeviceSrNo";
                lstSerialNo.ValueMember = "DeviceID";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
            finally
            {
                conn.Close();
            }
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            try
            {
                //Myla.SLN File Create
                string path = Path.Combine(Application.StartupPath, "Myla.SLN");
                if (!File.Exists(path))
                {
                    FileStream fs1 = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    fs1.Close();

                }             
                // If File is already Exists trucate and rewrite the Encrypted Value
                    var fs = new FileStream(path, FileMode.Truncate);
                    fs.Close();
                    foreach (DataRowView ite in lstSerialNo.Items)
                    {
                        string text = "";
                        text = ite.Row[lstSerialNo.DisplayMember].ToString();
                        if (text != null)
                        {
                            EnValue = Convert.ToString(text.ToString());
                            key = Convert.ToString(txtKey.Text);
                            if (key != "" && EnValue != "")
                            {
                                string encryption =EncryptionandDecryption.EncryptStringAES(EnValue, key);
                                File.AppendAllText(path, encryption + Environment.NewLine);
                            }
                        }

                    }
                    MessageBox.Show("Completed");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Informaion");
                return;
            }
            finally
            {
                conn.Close();
            }
        }
       
    }
}
