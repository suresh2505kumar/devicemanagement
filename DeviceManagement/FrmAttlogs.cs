﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using zkemkeeper;
using System.Security;
using System.Security.Cryptography;
using System.Runtime;

namespace DeviceManagement
{
    public partial class FrmAttlogs : Form
    {
        public FrmAttlogs()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand cmd;
        SqlDataAdapter da;
        public string DevId;
        public CZKEM axCZKEM1 = new zkemkeeper.CZKEM();
        private bool bIsConnected = false;//the boolean value identifies whether the device is connected
        private int iMachineNumber = 1;//the serial number of the device.After connecting the device ,this value will be changed.
        //private static Int32 MyCount = 0;
        //private static Int32 MyCountFinger = 0;
        //private static Int32 MyCountFace;
        private void FrmAttlogs_Load(object sender, EventArgs e)
        {
            txtKey.Text = "o7x8y6";
            loadip();
        }
        protected void loadip()
        {
            try
            {
                conn.Open();
                string query = "select DeviceIp,DeviceSrNo,License from MyDevices where Active=1";
                cmd = new SqlCommand(query, conn);
                da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                cmd.Dispose();
                da.Dispose();
                int i = 0;
                for (i = 0; i < dt.Rows.Count; i++)
                {
                    cmbip.Items.Add(dt.Rows[i]["DeviceIp"].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (cmbip.Text == "" || txtPort.Text == "")
            {
                MessageBox.Show("IP and Port cannot be null", "Error");
                return;
            }
            int idwErrorCode = 0;
            Cursor = Cursors.WaitCursor;
            if (btnConnect.Text == "DisConnect")
            {
                axCZKEM1.Disconnect();
                bIsConnected = false;
                btnConnect.Text = "Connect";
                axCZKEM1.RegEvent(iMachineNumber, 65535);
                lblstate.Text = "Current Status is disconnected";
                lblstate.ForeColor = Color.Red;
                Cursor = Cursors.Default;
                return;
            }
            //axCZKEM1.PullMode = 1;
            bIsConnected = axCZKEM1.Connect_Net(cmbip.Text, Convert.ToInt32(txtPort.Text));
            if (bIsConnected == true)
            {
                btnConnect.Text = "DisConnect";
                btnConnect.Refresh();
                lblstate.Text = "Current State is:Connected";
                lblstate.ForeColor = Color.Green;
                iMachineNumber = 1;
                axCZKEM1.RegEvent(iMachineNumber, 65535);
                //MyCountFinger = 1;
                //MyCountFace = 2;
            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");
            }
            Cursor = Cursors.Default;
        }

        private void btnDownloadLogs_Click(object sender, EventArgs e)
        {
            if (bIsConnected == false)
            {
                MessageBox.Show("Please connect the device first", "Error");
                return;
            }
            int idwErrorCode = 0;

            string sdwEnrollNumber = "";
            int idwVerifyMode = 0;
            int idwInOutMode = 0;
            int idwYear = 0;
            int idwMonth = 0;
            int idwDay = 0;
            int idwHour = 0;
            int idwMinute = 0;
            int idwSecond = 0;
            int idwWorkcode = 0;

            int iGLCount = 0;
            int iIndex = 0;
            Cursor = Cursors.WaitCursor;
            lvLogs.Items.Clear();
            axCZKEM1.EnableDevice(iMachineNumber, false);//disable the device
            if (axCZKEM1.ReadGeneralLogData(iMachineNumber))//read all the attendance records to the memory
            {
                while (axCZKEM1.SSR_GetGeneralLogData(iMachineNumber, out sdwEnrollNumber, out idwVerifyMode,
                            out idwInOutMode, out idwYear, out idwMonth, out idwDay, out idwHour, out idwMinute, out idwSecond, ref idwWorkcode))//get records from the memory
                {
                    iGLCount++;
                    lvLogs.Items.Add(iGLCount.ToString());
                    lvLogs.Items[iIndex].SubItems.Add(sdwEnrollNumber);//modify by Darcy on Nov.26 2009
                    lvLogs.Items[iIndex].SubItems.Add(idwVerifyMode.ToString());
                    lvLogs.Items[iIndex].SubItems.Add(idwInOutMode.ToString());
                    lvLogs.Items[iIndex].SubItems.Add(idwYear.ToString() + "-" + idwMonth.ToString() + "-" + idwDay.ToString() + " " + idwHour.ToString() + ":" + idwMinute.ToString() + ":" + idwSecond.ToString());
                    lvLogs.Items[iIndex].SubItems.Add(idwWorkcode.ToString());
                    iIndex++;

                    //save sqlserver devicelogs table
                    DateTime LogDate = Convert.ToDateTime(idwYear.ToString() + "-" + idwMonth.ToString() + "-" + idwDay.ToString() + " " + idwHour.ToString() + ":" + idwMinute.ToString() + ":" + idwSecond.ToString());
                    string ipaddress = cmbip.Text;

                    if (cmbip.Text != "")
                    {
                        string query1 = "Select * from Mydevices where DeviceIp='" + cmbip.Text + "'";
                        conn.Open();
                        SqlCommand cmd1 = new SqlCommand(query1, conn);
                        SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                        DataTable dt1 = new DataTable();
                        da1.Fill(dt1);
                        DevId = dt1.Rows[0]["DeviceId"].ToString();
                        conn.Close();

                    }
                    string query = "insert into Devicelogs (DownloadDate,DeviceId,UserId,LogDate,Direction,AttDirection,WorkCode,DeviceIp)values(@DownloadDate,@DeviceId,@UserId,@LogDate,@Direction,@AttDirection,@WorkCode,@DeviceIp)";
                    conn.Open();
                    cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@DownloadDate", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.AddWithValue("@DeviceId", SqlDbType.Int).Value = Convert.ToInt32(DevId.ToString());
                    cmd.Parameters.AddWithValue("@UserId", SqlDbType.NVarChar).Value = sdwEnrollNumber;
                    cmd.Parameters.AddWithValue("@LogDate", SqlDbType.DateTime).Value = LogDate;
                    cmd.Parameters.AddWithValue("@Direction", SqlDbType.NVarChar).Value = idwVerifyMode;
                    cmd.Parameters.AddWithValue("@AttDirection", SqlDbType.NVarChar).Value = idwInOutMode;
                    cmd.Parameters.AddWithValue("@WorkCode", SqlDbType.NVarChar).Value = idwWorkcode;
                    cmd.Parameters.AddWithValue("@DeviceIp", SqlDbType.NVarChar).Value = cmbip.Text.ToString();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    cmd.Dispose();
                }
            }
            else
            {
                Cursor = Cursors.Default;
                axCZKEM1.GetLastError(ref idwErrorCode);

                if (idwErrorCode != 0)
                {
                    MessageBox.Show("Reading data from terminal failed,ErrorCode: " + idwErrorCode.ToString(), "Error");
                }
                else
                {
                    MessageBox.Show("No data from terminal returns!", "Error");
                }
            }
            axCZKEM1.EnableDevice(iMachineNumber, true);//enable the device
            Cursor = Cursors.Default;
        }

        private void btnGetDeviceStatus_Click(object sender, EventArgs e)
        {
            if (bIsConnected == false)
            {
                MessageBox.Show("Please connect the device first", "Error");
                return;
            }
            int idwErrorCode = 0;
            int iValue = 0;

            axCZKEM1.EnableDevice(iMachineNumber, false);//disable the device
            if (axCZKEM1.GetDeviceStatus(iMachineNumber, 6, ref iValue)) //Here we use the function "GetDeviceStatus" to get the record's count.The parameter "Status" is 6.
            {
                MessageBox.Show("The count of the AttLogs in the device is " + iValue.ToString(), "Success");
            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                MessageBox.Show("Operation failed,ErrorCode=" + idwErrorCode.ToString(), "Error");
            }
            axCZKEM1.EnableDevice(iMachineNumber, true);//enable the device
        }

        private void btnClearGLog_Click(object sender, EventArgs e)
        {
            if (bIsConnected == false)
            {
                MessageBox.Show("Please connect the device first", "Error");
                return;
            }
            int idwErrorCode = 0;

            lvLogs.Items.Clear();
            axCZKEM1.EnableDevice(iMachineNumber, false);//disable the device
            if (axCZKEM1.ClearGLog(iMachineNumber))
            {
                axCZKEM1.RefreshData(iMachineNumber);//the data in the device should be refreshed
                MessageBox.Show("All att Logs have been cleared from teiminal!", "Success");
            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                MessageBox.Show("Operation failed,ErrorCode=" + idwErrorCode.ToString(), "Error");
            }
            axCZKEM1.EnableDevice(iMachineNumber, true);//enable the device
        }

        private void btnListClear_Click(object sender, EventArgs e)
        {
            lvLogs.Clear();
        }
    }
}
