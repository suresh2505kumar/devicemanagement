﻿namespace DeviceManagement
{
    partial class FrmAttlogs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDownloadLogs = new System.Windows.Forms.Button();
            this.grattlogs = new System.Windows.Forms.GroupBox();
            this.txtKey = new System.Windows.Forms.TextBox();
            this.lblstate = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.cmbip = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnConnect = new System.Windows.Forms.Button();
            this.lvLogs = new System.Windows.Forms.ListView();
            this.lvLogsch1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvLogsch2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvLogsch3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvLogsch4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvLogsch5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvLogsch6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvLogsch7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnGetDeviceStatus = new System.Windows.Forms.Button();
            this.btnClearGLog = new System.Windows.Forms.Button();
            this.btnListClear = new System.Windows.Forms.Button();
            this.grattlogs.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDownloadLogs
            // 
            this.btnDownloadLogs.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDownloadLogs.Location = new System.Drawing.Point(386, 52);
            this.btnDownloadLogs.Name = "btnDownloadLogs";
            this.btnDownloadLogs.Size = new System.Drawing.Size(153, 23);
            this.btnDownloadLogs.TabIndex = 0;
            this.btnDownloadLogs.Text = "Download AttLogs";
            this.btnDownloadLogs.UseVisualStyleBackColor = true;
            this.btnDownloadLogs.Click += new System.EventHandler(this.btnDownloadLogs_Click);
            // 
            // grattlogs
            // 
            this.grattlogs.BackColor = System.Drawing.SystemColors.Control;
            this.grattlogs.Controls.Add(this.txtKey);
            this.grattlogs.Controls.Add(this.lblstate);
            this.grattlogs.Controls.Add(this.label2);
            this.grattlogs.Controls.Add(this.txtPort);
            this.grattlogs.Controls.Add(this.cmbip);
            this.grattlogs.Controls.Add(this.label1);
            this.grattlogs.Location = new System.Drawing.Point(12, 12);
            this.grattlogs.Name = "grattlogs";
            this.grattlogs.Size = new System.Drawing.Size(371, 63);
            this.grattlogs.TabIndex = 1;
            this.grattlogs.TabStop = false;
            // 
            // txtKey
            // 
            this.txtKey.Enabled = false;
            this.txtKey.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKey.Location = new System.Drawing.Point(309, 11);
            this.txtKey.Name = "txtKey";
            this.txtKey.Size = new System.Drawing.Size(30, 22);
            this.txtKey.TabIndex = 5;
            this.txtKey.Text = "4370";
            this.txtKey.Visible = false;
            // 
            // lblstate
            // 
            this.lblstate.Location = new System.Drawing.Point(9, 38);
            this.lblstate.Name = "lblstate";
            this.lblstate.Size = new System.Drawing.Size(356, 19);
            this.lblstate.TabIndex = 4;
            this.lblstate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(238, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Port";
            // 
            // txtPort
            // 
            this.txtPort.Enabled = false;
            this.txtPort.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPort.Location = new System.Drawing.Point(273, 11);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(30, 22);
            this.txtPort.TabIndex = 2;
            this.txtPort.Text = "4370";
            // 
            // cmbip
            // 
            this.cmbip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbip.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbip.FormattingEnabled = true;
            this.cmbip.Location = new System.Drawing.Point(92, 9);
            this.cmbip.Name = "cmbip";
            this.cmbip.Size = new System.Drawing.Size(121, 23);
            this.cmbip.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ip address";
            // 
            // btnConnect
            // 
            this.btnConnect.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnect.Location = new System.Drawing.Point(386, 12);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(153, 23);
            this.btnConnect.TabIndex = 2;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // lvLogs
            // 
            this.lvLogs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvLogsch1,
            this.lvLogsch2,
            this.lvLogsch3,
            this.lvLogsch4,
            this.lvLogsch5,
            this.lvLogsch6,
            this.lvLogsch7});
            this.lvLogs.GridLines = true;
            this.lvLogs.Location = new System.Drawing.Point(12, 81);
            this.lvLogs.Name = "lvLogs";
            this.lvLogs.Size = new System.Drawing.Size(527, 362);
            this.lvLogs.TabIndex = 6;
            this.lvLogs.UseCompatibleStateImageBehavior = false;
            this.lvLogs.View = System.Windows.Forms.View.Details;
            // 
            // lvLogsch1
            // 
            this.lvLogsch1.Text = "Count";
            this.lvLogsch1.Width = 40;
            // 
            // lvLogsch2
            // 
            this.lvLogsch2.Text = "EnrollNumber";
            this.lvLogsch2.Width = 70;
            // 
            // lvLogsch3
            // 
            this.lvLogsch3.Text = "VerifyMode";
            this.lvLogsch3.Width = 84;
            // 
            // lvLogsch4
            // 
            this.lvLogsch4.Text = "InOutMode";
            // 
            // lvLogsch5
            // 
            this.lvLogsch5.Text = "Date";
            this.lvLogsch5.Width = 150;
            // 
            // lvLogsch6
            // 
            this.lvLogsch6.Text = "WorkCode";
            this.lvLogsch6.Width = 50;
            // 
            // lvLogsch7
            // 
            this.lvLogsch7.Text = "Reserved";
            this.lvLogsch7.Width = 50;
            // 
            // btnGetDeviceStatus
            // 
            this.btnGetDeviceStatus.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetDeviceStatus.Location = new System.Drawing.Point(12, 449);
            this.btnGetDeviceStatus.Name = "btnGetDeviceStatus";
            this.btnGetDeviceStatus.Size = new System.Drawing.Size(153, 23);
            this.btnGetDeviceStatus.TabIndex = 7;
            this.btnGetDeviceStatus.Text = "GetRecordsCount";
            this.btnGetDeviceStatus.UseVisualStyleBackColor = true;
            this.btnGetDeviceStatus.Click += new System.EventHandler(this.btnGetDeviceStatus_Click);
            // 
            // btnClearGLog
            // 
            this.btnClearGLog.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearGLog.Location = new System.Drawing.Point(187, 449);
            this.btnClearGLog.Name = "btnClearGLog";
            this.btnClearGLog.Size = new System.Drawing.Size(153, 23);
            this.btnClearGLog.TabIndex = 8;
            this.btnClearGLog.Text = "Clear Data To Machine";
            this.btnClearGLog.UseVisualStyleBackColor = true;
            this.btnClearGLog.Click += new System.EventHandler(this.btnClearGLog_Click);
            // 
            // btnListClear
            // 
            this.btnListClear.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListClear.Location = new System.Drawing.Point(356, 449);
            this.btnListClear.Name = "btnListClear";
            this.btnListClear.Size = new System.Drawing.Size(153, 23);
            this.btnListClear.TabIndex = 9;
            this.btnListClear.Text = "Clear Data to List";
            this.btnListClear.UseVisualStyleBackColor = true;
            this.btnListClear.Click += new System.EventHandler(this.btnListClear_Click);
            // 
            // FrmAttlogs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(547, 477);
            this.Controls.Add(this.btnListClear);
            this.Controls.Add(this.btnClearGLog);
            this.Controls.Add(this.btnGetDeviceStatus);
            this.Controls.Add(this.lvLogs);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.grattlogs);
            this.Controls.Add(this.btnDownloadLogs);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAttlogs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Attendance Logs Downloder";
            this.Load += new System.EventHandler(this.FrmAttlogs_Load);
            this.grattlogs.ResumeLayout(false);
            this.grattlogs.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDownloadLogs;
        private System.Windows.Forms.GroupBox grattlogs;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.ComboBox cmbip;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Label lblstate;
        private System.Windows.Forms.ListView lvLogs;
        private System.Windows.Forms.ColumnHeader lvLogsch1;
        private System.Windows.Forms.ColumnHeader lvLogsch2;
        private System.Windows.Forms.ColumnHeader lvLogsch3;
        private System.Windows.Forms.ColumnHeader lvLogsch4;
        private System.Windows.Forms.ColumnHeader lvLogsch5;
        private System.Windows.Forms.ColumnHeader lvLogsch6;
        private System.Windows.Forms.ColumnHeader lvLogsch7;
        private System.Windows.Forms.TextBox txtKey;
        private System.Windows.Forms.Button btnGetDeviceStatus;
        private System.Windows.Forms.Button btnClearGLog;
        private System.Windows.Forms.Button btnListClear;
    }
}