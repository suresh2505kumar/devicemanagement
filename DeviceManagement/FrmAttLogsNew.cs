﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using zkemkeeper;

namespace DeviceManagement
{
    public partial class FrmAttLogsNew : Form
    {
        public FrmAttLogsNew()
        {
            InitializeComponent();
            backWorkerAttlogs.DoWork += BackWorkerAttlogs_DoWork;
        }
        SQLDBHelper db = new SQLDBHelper();
        public CZKEM axCZKEM1 = new zkemkeeper.CZKEM();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand cmd;
        private bool bIsConnected = false;//the boolean value identifies whether the device is connected
        private int iMachineNumber = 1;
        private int idwErrorCode = 0;
        private void FrmAttLogsNew_Load(object sender, EventArgs e)
        {
            try
            {
                LoadData();
                Loaddata();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void LoadData()
        {
            try
            {
                DataGridAttLogs.DataSource = null;
                DataGridAttLogs.AutoGenerateColumns = false;
                DataGridAttLogs.ColumnCount = 8;
                DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn
                {
                    HeaderText = "chck",
                    Width = 50,
                    Name = "checkBoxColumn"
                };
                DataGridAttLogs.Columns.Insert(0, checkBoxColumn);
                DataGridAttLogs.Columns[1].Name = "Device Name";
                DataGridAttLogs.Columns[1].HeaderText = "Device Name";
                DataGridAttLogs.Columns[2].Width = 150;
                DataGridAttLogs.Columns[2].Name = "IpAddress";
                DataGridAttLogs.Columns[2].HeaderText = "Ip Address";
                DataGridAttLogs.Columns[2].Width = 140;
                DataGridAttLogs.Columns[3].Name = "Location";
                DataGridAttLogs.Columns[3].HeaderText = "Location";
                DataGridAttLogs.Columns[3].Width = 120;
                DataGridAttLogs.Columns[4].Name = "LastDownloadDate";
                DataGridAttLogs.Columns[4].HeaderText = "Last Download Date";
                DataGridAttLogs.Columns[4].Width = 200;
                DataGridAttLogs.Columns[5].Name = "Uid";
                DataGridAttLogs.Columns[5].HeaderText = "Uid";
                DataGridAttLogs.Columns[5].Visible = false;
                DataGridAttLogs.Columns[6].Name = "Logcount";
                DataGridAttLogs.Columns[6].HeaderText = "Logs Count";
                DataGridAttLogs.Columns[7].Name = "Status";
                DataGridAttLogs.Columns[7].HeaderText = "Status";
                DataGridAttLogs.Columns[7].Width = 150;
                DataGridAttLogs.Columns[8].Name = "DeviceType";
                DataGridAttLogs.Columns[8].HeaderText = "DeviceType";
                DataGridAttLogs.Columns[8].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void Loaddata()
        {
            try
            {
                DataTable data = db.getData("SP_GetDevices");
                DataTable dt = data.Select("Location='" + GlobalVar.Branch + "'", null).CopyToDataTable();
                if (dt.Rows.Count != 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        int Index = DataGridAttLogs.Rows.Add();
                        DataGridAttLogs.Rows[Index].Cells[1].Value = dt.Rows[i]["DeviceName"].ToString();
                        DataGridAttLogs.Rows[Index].Cells[2].Value = dt.Rows[i]["Deviceip"].ToString();
                        DataGridAttLogs.Rows[Index].Cells[3].Value = dt.Rows[i]["Location"].ToString();
                        DataGridAttLogs.Rows[Index].Cells[7].Value = "";
                        DataGridAttLogs.Rows[Index].Cells[5].Value = dt.Rows[i]["DeviceID"].ToString();
                        DataGridAttLogs.Rows[Index].Cells[8].Value = dt.Rows[i]["DeviceType"].ToString();
                    }
                }
                DataSet ds = db.getMultipleDataTableWithoutPara(CommandType.StoredProcedure, "SP_getLastRecordCount");
                DataTable dt1 = ds.Tables[0];
                //DataTable dt2 = ds.Tables[1];
                for (int k = 0; k < DataGridAttLogs.Rows.Count; k++)
                {
                    string gridip = DataGridAttLogs.Rows[k].Cells["IpAddress"].Value.ToString();
                    DataTable dataTable = dt1.Select("DeviceIp ='" + gridip + "'").CopyToDataTable();
                    if(dataTable.Rows.Count != 0)
                    {
                        DataGridAttLogs.Rows[k].Cells[4].Value = (DateTime)dataTable.Rows[0]["LastLogDate"];
                    }
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //return;
            }
        }

        private void DataGridAttLogs_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnConnect_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow dataGridRow in DataGridAttLogs.Rows)
                {
                    if (dataGridRow.Cells["checkBoxColumn"].Value != null && (bool)dataGridRow.Cells["checkBoxColumn"].Value)
                    {
                        if (dataGridRow.Cells["IpAddress"].Value == null)
                        {
                            MessageBox.Show("IP and Port cannot be null", "Error");
                            return;
                        }
                        string ip = dataGridRow.Cells["IpAddress"].Value.ToString();
                        int idwErrorCode = 0;
                        Cursor = Cursors.WaitCursor;
                        LogFile.WriteLog("Macine Test Connection initiated " + ip + " - " + DateTime.Now + "");
                        bIsConnected = axCZKEM1.Connect_Net(ip, Convert.ToInt32(4370));
                        if (bIsConnected == true)
                        {
                            DataGridAttLogs.ClearSelection();
                            btnConnect.Refresh();
                            dataGridRow.Cells["Status"].Value = "Connection Successfull";
                            LogFile.WriteLog("Macine Connected " + ip + " - " + DateTime.Now + "");
                            dataGridRow.Cells["Status"].Style.BackColor = Color.Green;
                            iMachineNumber = 1;
                            axCZKEM1.RegEvent(iMachineNumber, 65535);
                        }
                        else
                        {

                            axCZKEM1.GetLastError(ref idwErrorCode);
                            dataGridRow.Cells["Status"].Value = "Connection Un Successfull";
                            LogFile.WriteLog("Macine Not Connected " + ip + " - " + idwErrorCode + DateTime.Now + "");
                            dataGridRow.Cells["Status"].Style.BackColor = Color.Red;
                        }
                        Cursor = Cursors.Default;
                        btnConnect.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnDownloadLogs_Click(object sender, EventArgs e)
        {
            try
            {
                backWorkerAttlogs.RunWorkerAsync(2000);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BackWorkerAttlogs_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                BackgroundWorker helperBW = sender as BackgroundWorker;
                int arg = (int)e.Argument;
                e.Result = BackgroundLogicMethod(helperBW, arg);
                if (helperBW.CancellationPending)
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected int BackgroundLogicMethod(BackgroundWorker bw, int a)
        {
            int result = 0;
            try
            {
                foreach (DataGridViewRow dataGridRow in DataGridAttLogs.Rows)
                {
                    int machineId = Convert.ToInt32(dataGridRow.Cells["Uid"].Value.ToString());
                    if (dataGridRow.Cells["checkBoxColumn"].Value != null && (bool)dataGridRow.Cells["checkBoxColumn"].Value)
                    {
                        if (dataGridRow.Cells["IpAddress"].Value == null)
                        {
                            MessageBox.Show("IP and Port cannot be null", "Error");
                        }
                        string ip = dataGridRow.Cells["IpAddress"].Value.ToString();
                        int idwErrorCode = 0;
                        dataGridRow.Cells["Status"].Value = "Conecting...";
                        LogFile.WriteLog("Macine Connection initiated " + ip + " - " + DateTime.Now + "");
                        bIsConnected = axCZKEM1.Connect_Net(ip, Convert.ToInt32(4370));
                        if (bIsConnected == true)
                        {
                            DataGridAttLogs.ClearSelection();
                            dataGridRow.Cells["Status"].Value = "Connected";
                            LogFile.WriteLog("Macine Connected " + ip + " - " + DateTime.Now + "");
                            dataGridRow.Cells["Status"].Style.BackColor = Color.Green;
                            iMachineNumber = 1;
                            axCZKEM1.RegEvent(iMachineNumber, 65535);

                            if (bIsConnected == false)
                            {
                                MessageBox.Show("Please connect the device first", "Error");
                            }
                            string sdwEnrollNumber = "";
                            int idwVerifyMode = 0;
                            int idwInOutMode = 0;
                            int idwYear = 0;
                            int idwMonth = 0;
                            int idwDay = 0;
                            int idwHour = 0;
                            int idwMinute = 0;
                            int idwSecond = 0;
                            int idwWorkcode = 0;
                            int iGLCount = 0;
                            int iIndex = 0;
                            int iValue = 0;
                            int CountDgv = 0;
                            DateTime lLogdate = new DateTime();
                            int LUserId =0 ;
                            CountDgv = 0;
                            if (dataGridRow.Cells[6].Value == null || dataGridRow.Cells[6].Value.ToString() == "")
                            {
                                CountDgv = 0;
                            }
                            else
                            {
                                CountDgv = Convert.ToInt32(dataGridRow.Cells[6].Value.ToString());
                            }
                            axCZKEM1.EnableDevice(iMachineNumber, false);//disable the device
                            axCZKEM1.GetDeviceStatus(iMachineNumber, 6, ref iValue);
                            LogFile.WriteLog("Log Records feteching.... " + ip + " - " + DateTime.Now + "");
                            if (axCZKEM1.ReadGeneralLogData(iMachineNumber))//read all the attendance records to the memory
                            {
                                LogFile.WriteLog("Log Records Downloading.... " + ip + " - " + DateTime.Now + "");
                                dataGridRow.Cells["Status"].Value = "Downloading...";
                                while (axCZKEM1.SSR_GetGeneralLogData(iMachineNumber, out sdwEnrollNumber, out idwVerifyMode, out idwInOutMode, out idwYear, out idwMonth, out idwDay, out idwHour, out idwMinute, out idwSecond, ref idwWorkcode))//get records from the memory
                                {

                                    iGLCount++;
                                    LUserId = Convert.ToInt32(sdwEnrollNumber.ToString());
                                    dataGridRow.Cells[4].Value = idwYear.ToString() + "-" + idwMonth.ToString() + "-" + idwDay.ToString() + " " + idwHour.ToString() + ":" + idwMinute.ToString() + ":" + idwSecond.ToString();
                                    iIndex++;
                                    DateTime LogDate = Convert.ToDateTime(idwYear.ToString() + "-" + idwMonth.ToString() + "-" + idwDay.ToString() + " " + idwHour.ToString() + ":" + idwMinute.ToString() + ":" + idwSecond.ToString());
                                    lLogdate = LogDate;
                                    conn.Open();
                                    cmd = new SqlCommand();
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Connection = conn;
                                    cmd.CommandText = "SP_InsertAttLogs";
                                    cmd.Parameters.AddWithValue("@DownloadDate", SqlDbType.DateTime).Value = DateTime.Now;
                                    cmd.Parameters.AddWithValue("@DeviceId", SqlDbType.Int).Value = machineId;
                                    cmd.Parameters.AddWithValue("@UserId", SqlDbType.NVarChar).Value = sdwEnrollNumber.ToString();
                                    cmd.Parameters.AddWithValue("@LogDate", SqlDbType.DateTime).Value = LogDate;
                                    cmd.Parameters.AddWithValue("@Direction", SqlDbType.NVarChar).Value = idwVerifyMode;
                                    cmd.Parameters.AddWithValue("@AttDirection", SqlDbType.NVarChar).Value = idwInOutMode;
                                    cmd.Parameters.AddWithValue("@WorkCode", SqlDbType.NVarChar).Value = GlobalVar.Branch;
                                    cmd.Parameters.AddWithValue("@DeviceIp", SqlDbType.NVarChar).Value = dataGridRow.Cells["IpAddress"].Value.ToString();
                                    if (idwInOutMode == 0)
                                    {
                                        cmd.Parameters.AddWithValue("@C1", SqlDbType.NVarChar).Value = "i";
                                    }
                                    else if (idwInOutMode == 1)
                                    {
                                        cmd.Parameters.AddWithValue("@C1", SqlDbType.NVarChar).Value = "o";
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@C1", SqlDbType.NVarChar).Value = "i";
                                    }
                                    cmd.ExecuteNonQuery();
                                    conn.Close();
                                    CountDgv++;
                                    dataGridRow.Cells[6].Value = CountDgv;
                                }
                                LogFile.WriteLog("Log Records Downloading Completed " + ip + " - " + DateTime.Now + "");
                            }
                            else
                            {
                                axCZKEM1.GetLastError(ref idwErrorCode);
                                LogFile.WriteLog("No Records Found " + ip + " - " + DateTime.Now + "");
                                if (idwErrorCode != 0)
                                {
                                    MessageBox.Show("Reading data from terminal failed,ErrorCode: " + idwErrorCode.ToString(), "Error");
                                }
                                else
                                {
                                    MessageBox.Show("No data from terminal returns!", "Error");
                                }
                            }
                            if (chckAutoClear.Checked == true)
                            {
                                //Clear Attendance Data
                                int DeviceLUserId = 0;
                                DateTime DeviceLLogDate = new DateTime();
                                if (axCZKEM1.ReadGeneralLogData(iMachineNumber))//read all the attendance records to the memory
                                {
                                    while (axCZKEM1.SSR_GetGeneralLogData(iMachineNumber, out sdwEnrollNumber, out idwVerifyMode, out idwInOutMode, out idwYear, out idwMonth, out idwDay, out idwHour, out idwMinute, out idwSecond, ref idwWorkcode))//get records from the memory
                                    {
                                        DeviceLUserId = Convert.ToInt32(sdwEnrollNumber.ToString());
                                        DeviceLLogDate = Convert.ToDateTime(idwYear.ToString() + "-" + idwMonth.ToString() + "-" + idwDay.ToString() + " " + idwHour.ToString() + ":" + idwMinute.ToString() + ":" + idwSecond.ToString());
                                    }
                                    if (DeviceLUserId == LUserId && DeviceLLogDate == lLogdate)
                                    {
                                        axCZKEM1.ClearGLog(iMachineNumber);
                                        LogFile.WriteLog("Records Cleared " + ip + " - " + DateTime.Now + "");
                                    }
                                }
                                else
                                {
                                    LogFile.WriteLog("Records not Clear " + ip + " - " + DateTime.Now + "");
                                }
                                SqlCommand Comm1 = new SqlCommand("update Devicelogs set c7 ='Data Cleared' where ipaddress = '" + ip + "' and Userid= " + DeviceLUserId + " and Logdate ='" + DeviceLLogDate + "'", conn);
                            }
                            axCZKEM1.EnableDevice(iMachineNumber, true);//enable the device
                            axCZKEM1.Disconnect();
                            bIsConnected = false;
                            dataGridRow.Cells["Status"].Value = "Disconnected";
                            dataGridRow.Cells["Status"].Style.BackColor = Color.White;
                        }
                        else
                        {
                            axCZKEM1.GetLastError(ref idwErrorCode);
                            MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        private void BtnDownloadToUSB_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "DAT Files|*.dat;";
                openFileDialog.ShowDialog();
                try
                {
                    if (File.Exists(openFileDialog.FileName))// only executes if the file at pathtofile exists//you need to add the using System.IO reference at the top of te code to use this
                    {
                        string text = "";
                        using (StreamReader sr = new StreamReader(openFileDialog.FileName))
                        {
                            string[] stringSeparators = new string[] { "\r\n" };
                            string[] stringSeparatorsNew = new string[] { "\t" };
                            text = sr.ReadToEnd();//all text wil be saved in text enters are also saved
                            string[] vs = text.Split(stringSeparators, StringSplitOptions.None);
                            for (int i = 0; i < vs.Length; i++)
                            {
                                string t = vs[i];
                                if (t.Length != 0)
                                {
                                    string[] log = t.Split(stringSeparatorsNew, StringSplitOptions.None);
                                    string empced = "";
                                    string dateTime = "";
                                    string deviceid = "";
                                    if (log.Length == 11)
                                    {
                                        empced = log[4];
                                        dateTime = log[5] + " " + log[6];
                                        deviceid = log[7];
                                    }
                                    else
                                    {
                                        empced = log[0].Trim();
                                        dateTime = log[1];
                                        deviceid = log[2];
                                    }
                                    DateTime logdate = Convert.ToDateTime(dateTime);
                                    string Query = "select * from Tdevicelogs Where UserId ='" + empced + "' and LogDate ='" + logdate + "'";
                                    cmd = new SqlCommand(Query, conn);
                                    SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                                    DataTable dataTable = new DataTable();
                                    dataAdapter.Fill(dataTable);
                                    if (dataTable.Rows.Count == 0)
                                    {
                                        cmd = new SqlCommand();
                                        cmd.CommandType = CommandType.StoredProcedure;
                                        cmd.Connection = conn;
                                        cmd.CommandText = "SP_InsertAttLogs";
                                        cmd.Parameters.AddWithValue("@DownloadDate", SqlDbType.DateTime).Value = DateTime.Now;
                                        cmd.Parameters.AddWithValue("@DeviceId", SqlDbType.Int).Value = deviceid;
                                        cmd.Parameters.AddWithValue("@UserId", SqlDbType.NVarChar).Value = empced;
                                        cmd.Parameters.AddWithValue("@LogDate", SqlDbType.DateTime).Value = logdate;
                                        cmd.Parameters.AddWithValue("@Direction", SqlDbType.NVarChar).Value = "0";
                                        cmd.Parameters.AddWithValue("@AttDirection", SqlDbType.NVarChar).Value = "0";
                                        cmd.Parameters.AddWithValue("@WorkCode", SqlDbType.NVarChar).Value = GlobalVar.Branch;
                                        cmd.Parameters.AddWithValue("@DeviceIp", SqlDbType.NVarChar).Value = "USB";
                                        cmd.Parameters.AddWithValue("@C1", SqlDbType.NVarChar).Value = "i";
                                        if (conn.State == ConnectionState.Closed)
                                        {
                                            conn.Open();
                                        }
                                        cmd.ExecuteNonQuery();
                                        conn.Close();
                                    }
                                }
                            }
                            MessageBox.Show("Upload Successfully !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
