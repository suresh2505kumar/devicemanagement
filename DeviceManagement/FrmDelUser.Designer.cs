﻿namespace DeviceManagement
{
    partial class FrmDelUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grattlogs = new System.Windows.Forms.GroupBox();
            this.txtKey = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.cmbip = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblstate = new System.Windows.Forms.Label();
            this.dgvorginal = new System.Windows.Forms.DataGridView();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.txtUserid = new System.Windows.Forms.TextBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.DataGridToUsers = new System.Windows.Forms.DataGridView();
            this.btnForward = new System.Windows.Forms.Button();
            this.btnBackward = new System.Windows.Forms.Button();
            this.chckIpList = new System.Windows.Forms.CheckedListBox();
            this.chckSelectIp = new System.Windows.Forms.CheckBox();
            this.chckSelectAllEmp = new System.Windows.Forms.CheckBox();
            this.grattlogs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvorginal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridToUsers)).BeginInit();
            this.SuspendLayout();
            // 
            // grattlogs
            // 
            this.grattlogs.BackColor = System.Drawing.SystemColors.Control;
            this.grattlogs.Controls.Add(this.txtKey);
            this.grattlogs.Controls.Add(this.label2);
            this.grattlogs.Controls.Add(this.txtPort);
            this.grattlogs.Controls.Add(this.cmbip);
            this.grattlogs.Controls.Add(this.label1);
            this.grattlogs.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grattlogs.Location = new System.Drawing.Point(428, 8);
            this.grattlogs.Name = "grattlogs";
            this.grattlogs.Size = new System.Drawing.Size(220, 71);
            this.grattlogs.TabIndex = 10;
            this.grattlogs.TabStop = false;
            this.grattlogs.Visible = false;
            // 
            // txtKey
            // 
            this.txtKey.Enabled = false;
            this.txtKey.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKey.Location = new System.Drawing.Point(168, 18);
            this.txtKey.Name = "txtKey";
            this.txtKey.Size = new System.Drawing.Size(30, 26);
            this.txtKey.TabIndex = 5;
            this.txtKey.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(258, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Port";
            this.label2.Visible = false;
            // 
            // txtPort
            // 
            this.txtPort.Enabled = false;
            this.txtPort.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPort.Location = new System.Drawing.Point(251, 15);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(41, 26);
            this.txtPort.TabIndex = 2;
            this.txtPort.Text = "4370";
            this.txtPort.Visible = false;
            // 
            // cmbip
            // 
            this.cmbip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbip.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbip.FormattingEnabled = true;
            this.cmbip.Location = new System.Drawing.Point(9, 35);
            this.cmbip.Name = "cmbip";
            this.cmbip.Size = new System.Drawing.Size(121, 26);
            this.cmbip.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ip address";
            // 
            // lblstate
            // 
            this.lblstate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblstate.Location = new System.Drawing.Point(340, 82);
            this.lblstate.Name = "lblstate";
            this.lblstate.Size = new System.Drawing.Size(307, 18);
            this.lblstate.TabIndex = 4;
            this.lblstate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblstate.Visible = false;
            // 
            // dgvorginal
            // 
            this.dgvorginal.AllowUserToAddRows = false;
            this.dgvorginal.BackgroundColor = System.Drawing.Color.White;
            this.dgvorginal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvorginal.Location = new System.Drawing.Point(10, 134);
            this.dgvorginal.Name = "dgvorginal";
            this.dgvorginal.RowHeadersVisible = false;
            this.dgvorginal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvorginal.Size = new System.Drawing.Size(299, 347);
            this.dgvorginal.TabIndex = 9;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Location = new System.Drawing.Point(253, 485);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(153, 26);
            this.BtnDelete.TabIndex = 14;
            this.BtnDelete.Text = "Delete Users";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // txtUserid
            // 
            this.txtUserid.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserid.Location = new System.Drawing.Point(11, 105);
            this.txtUserid.Name = "txtUserid";
            this.txtUserid.Size = new System.Drawing.Size(298, 26);
            this.txtUserid.TabIndex = 12;
            this.txtUserid.TextChanged += new System.EventHandler(this.TxtUserid_TextChanged);
            // 
            // btnConnect
            // 
            this.btnConnect.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnect.Location = new System.Drawing.Point(234, 12);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(107, 25);
            this.btnConnect.TabIndex = 11;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Visible = false;
            this.btnConnect.Click += new System.EventHandler(this.BtnConnect_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoad.Location = new System.Drawing.Point(233, 13);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(107, 26);
            this.btnLoad.TabIndex = 15;
            this.btnLoad.Text = "Load Users";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.BtnLoad_Click);
            // 
            // DataGridToUsers
            // 
            this.DataGridToUsers.AllowUserToAddRows = false;
            this.DataGridToUsers.BackgroundColor = System.Drawing.Color.White;
            this.DataGridToUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridToUsers.Location = new System.Drawing.Point(347, 134);
            this.DataGridToUsers.Name = "DataGridToUsers";
            this.DataGridToUsers.RowHeadersVisible = false;
            this.DataGridToUsers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridToUsers.Size = new System.Drawing.Size(299, 346);
            this.DataGridToUsers.TabIndex = 16;
            // 
            // btnForward
            // 
            this.btnForward.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnForward.Location = new System.Drawing.Point(315, 237);
            this.btnForward.Name = "btnForward";
            this.btnForward.Size = new System.Drawing.Size(26, 25);
            this.btnForward.TabIndex = 17;
            this.btnForward.Text = ">";
            this.btnForward.UseVisualStyleBackColor = true;
            this.btnForward.Click += new System.EventHandler(this.BtnForward_Click);
            // 
            // btnBackward
            // 
            this.btnBackward.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackward.Location = new System.Drawing.Point(315, 277);
            this.btnBackward.Name = "btnBackward";
            this.btnBackward.Size = new System.Drawing.Size(26, 25);
            this.btnBackward.TabIndex = 18;
            this.btnBackward.Text = "<";
            this.btnBackward.UseVisualStyleBackColor = true;
            this.btnBackward.Click += new System.EventHandler(this.BtnBackward_Click);
            // 
            // chckIpList
            // 
            this.chckIpList.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckIpList.FormattingEnabled = true;
            this.chckIpList.Location = new System.Drawing.Point(12, 8);
            this.chckIpList.Name = "chckIpList";
            this.chckIpList.Size = new System.Drawing.Size(215, 88);
            this.chckIpList.TabIndex = 19;
            // 
            // chckSelectIp
            // 
            this.chckSelectIp.AutoSize = true;
            this.chckSelectIp.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckSelectIp.Location = new System.Drawing.Point(233, 74);
            this.chckSelectIp.Name = "chckSelectIp";
            this.chckSelectIp.Size = new System.Drawing.Size(100, 22);
            this.chckSelectIp.TabIndex = 20;
            this.chckSelectIp.Text = "Select All Ip";
            this.chckSelectIp.UseVisualStyleBackColor = true;
            this.chckSelectIp.CheckedChanged += new System.EventHandler(this.ChckSelectIp_CheckedChanged);
            // 
            // chckSelectAllEmp
            // 
            this.chckSelectAllEmp.AutoSize = true;
            this.chckSelectAllEmp.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckSelectAllEmp.Location = new System.Drawing.Point(10, 487);
            this.chckSelectAllEmp.Name = "chckSelectAllEmp";
            this.chckSelectAllEmp.Size = new System.Drawing.Size(85, 22);
            this.chckSelectAllEmp.TabIndex = 21;
            this.chckSelectAllEmp.Text = "Select All";
            this.chckSelectAllEmp.UseVisualStyleBackColor = true;
            this.chckSelectAllEmp.CheckedChanged += new System.EventHandler(this.ChckSelectAllEmp_CheckedChanged);
            // 
            // FrmDelUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(661, 516);
            this.Controls.Add(this.chckSelectAllEmp);
            this.Controls.Add(this.chckSelectIp);
            this.Controls.Add(this.chckIpList);
            this.Controls.Add(this.btnBackward);
            this.Controls.Add(this.lblstate);
            this.Controls.Add(this.btnForward);
            this.Controls.Add(this.DataGridToUsers);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.grattlogs);
            this.Controls.Add(this.dgvorginal);
            this.Controls.Add(this.BtnDelete);
            this.Controls.Add(this.txtUserid);
            this.Controls.Add(this.btnConnect);
            this.Name = "FrmDelUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Delete users in Device";
            this.Load += new System.EventHandler(this.FrmDelUser_Load);
            this.grattlogs.ResumeLayout(false);
            this.grattlogs.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvorginal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridToUsers)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grattlogs;
        private System.Windows.Forms.TextBox txtKey;
        private System.Windows.Forms.Label lblstate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.ComboBox cmbip;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvorginal;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.TextBox txtUserid;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.DataGridView DataGridToUsers;
        private System.Windows.Forms.Button btnForward;
        private System.Windows.Forms.Button btnBackward;
        private System.Windows.Forms.CheckedListBox chckIpList;
        private System.Windows.Forms.CheckBox chckSelectIp;
        private System.Windows.Forms.CheckBox chckSelectAllEmp;
    }
}