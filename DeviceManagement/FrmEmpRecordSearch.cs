﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DeviceManagement
{
    public partial class FrmEmpRecordSearch : Form
    {
        public FrmEmpRecordSearch()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SQLDBHelper db = new SQLDBHelper();
        private void grFront_Enter(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtEmpCodeSearch.Text != string.Empty)
                {
                    SqlParameter[] parameters = {
                        new SqlParameter("@UserID",txtEmpCodeSearch.Text),
                        new SqlParameter("@Fromdate",Convert.ToDateTime(dtpFromDate.Text)),
                        new SqlParameter("@ToDate",Convert.ToDateTime(DtpToDate.Text)),
                    };
                    DataTable dt = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "Proc_GetLogs", parameters);
                    if(dt.Rows.Count > 0)
                    {
                        DataTable dataTable = dt.Select("Location='" + GlobalVar.Branch + "'", null).CopyToDataTable();
                        DataGridSearch.Rows.Clear();
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            int Index = DataGridSearch.Rows.Add();
                            DataGridViewRow row = (DataGridViewRow)DataGridSearch.Rows[Index];
                            row.Cells[0].Value = dataTable.Rows[i]["Userid"].ToString();
                            row.Cells[1].Value = dataTable.Rows[i]["Logdate"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,ex.StackTrace);
                return;
            }
        }

        protected void LoadLogs()
        {
            try
            {
                DataGridSearch.AutoGenerateColumns = false;
                DataGridSearch.DataSource = null;
                DataGridSearch.ColumnCount = 2;
                DataGridSearch.Columns[0].Name = "Employee Code";
                DataGridSearch.Columns[0].HeaderText = "Employee Code";
                DataGridSearch.Columns[0].Width = 150;

                DataGridSearch.Columns[1].Name = "LogDate";
                DataGridSearch.Columns[1].HeaderText = "LogDate";
                DataGridSearch.Columns[1].Width = 300;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void FrmEmpRecordSearch_Load(object sender, EventArgs e)
        {
            LoadLogs();
        }
    }
}
