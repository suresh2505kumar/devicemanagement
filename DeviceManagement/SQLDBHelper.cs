﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace DeviceManagement
{
    public class SQLDBHelper
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        public int ExecuteQuery(string ProcedureName, SqlParameter[] para)
        {
            int id = 0;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = ProcedureName;
                cmd.Connection = conn;
                cmd.Parameters.AddRange(para);
                id = cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw ex;
            }
            return id;
        }
        public DataTable getData(string ProcedureName)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = ProcedureName;
                cmd.Connection = conn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataSet getMultipleDataTable(CommandType CmdType, string CommandText, SqlParameter[] para)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CmdType;
                cmd.CommandText = CommandText;
                cmd.Connection = conn;
                cmd.Parameters.AddRange(para);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public DataSet getMultipleDataTableWithoutPara(CommandType CmdType, string CommandText)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CmdType;
                cmd.CommandText = CommandText;
                cmd.Connection = conn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public DataTable getDataWithParameter(string ProcedureName, SqlParameter[] para)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = ProcedureName;
                cmd.Connection = conn;
                cmd.Parameters.AddRange(para);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable getDataWithParameterCmdType(CommandType cmdType, string CommandText, SqlParameter[] para)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = CommandText;
                cmd.Connection = conn;
                cmd.Parameters.AddRange(para);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception)
            {

            }
            return dt;
        }
        public DataTable getDataCmdType(CommandType cmdType, string CommandText)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = CommandText;
                cmd.Connection = conn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public int ExecuteScalar(CommandType cmdType, string CommandText, SqlParameter[] para, int retunIndex)
        {
            int ReturnUid = 0;
            try
            {
                conn.Close();
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = CommandText;
                cmd.Parameters.AddRange(para);
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
                ReturnUid = Convert.ToInt32(cmd.Parameters[retunIndex].Value.ToString());
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ReturnUid;
        }
    }
}
