﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using System.Configuration;
using System.IO;

namespace DeviceManagement
{
    public partial class Frmdevices : Form
    {

        public Frmdevices()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        string key, EnValue;
        SqlConnection conn;
        SqlCommand cmd;
        SqlDataAdapter da;
        public zkemkeeper.CZKEM axCZKEM1 = new zkemkeeper.CZKEM();
        private bool bIsConnected = false;//the boolean value identifies whether the device is connected
        private int iMachineNumber = 1;//the serial number of the device.After connecting the device ,this value will be changed.
        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frmdevices_Load(object sender, EventArgs e)
        {
            Load_grid();
            txtKey.Text = "o7x8y6";
            txtLocation.Text = GlobalVar.Branch;
        }

        private void lnklblSerNo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (TxtIp.Text.Trim() == "" || txtPort.Text.Trim() == "")
            {
                MessageBox.Show("IP and Port cannot be null", "Error");
                return;
            }
            int idwErrorCode = 0;
            Cursor = Cursors.WaitCursor;
            bIsConnected = axCZKEM1.Connect_Net(TxtIp.Text.Trim(), Convert.ToInt32(txtPort.Text.Trim()));
            if (bIsConnected == true)
            {
                iMachineNumber = 1;
                axCZKEM1.RegEvent(iMachineNumber, 65535);
                string serialnum;
                axCZKEM1.GetSerialNumber(iMachineNumber, out serialnum);
                TxtSerialNo.Text = serialnum;
                axCZKEM1.Disconnect();
                Cursor = Cursors.Default;
            }
            else
            {
                axCZKEM1.GetLastError(ref idwErrorCode);
                MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");
            }
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {

            if (TxtDeviceName.Text == "")
            {
                MessageBox.Show("Enter the Device name", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                TxtDeviceName.Focus();
            }
            else if (TxtIp.Text == "")
            {
                MessageBox.Show("Enter the Device IPaddress", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                TxtIp.Focus();
            }
            else if (cmbDirection.Text == "")
            {
                MessageBox.Show("Select Device Direction", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmbDirection.Focus();
            }
            else if (cmbDeviceType.Text == "")
            {
                MessageBox.Show("Select Device Type", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmbDirection.Focus();
            }
            else
            {
                conn.Open();
                cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = conn;
                cmd.CommandText = "SP_Mydevices";
                cmd.Parameters.AddWithValue("@DeviceName", SqlDbType.NVarChar).Value = TxtDeviceName.Text;
                cmd.Parameters.AddWithValue("@DeviceIp", SqlDbType.NVarChar).Value = TxtIp.Text;
                cmd.Parameters.AddWithValue("@DeviceDir", SqlDbType.NVarChar).Value = cmbDirection.Text;
                cmd.Parameters.AddWithValue("@DeviceSrNo", SqlDbType.NVarChar).Value = TxtSerialNo.Text;
                cmd.Parameters.AddWithValue("@Active", SqlDbType.Bit).Value = true;
                cmd.Parameters.AddWithValue("@Location", SqlDbType.NVarChar).Value = txtLocation.Text;
                cmd.Parameters.AddWithValue("@DeviceType", SqlDbType.NVarChar).Value = cmbDeviceType.Text;
                if (BtnSave.Text == "Save")
                {
                    cmd.Parameters.AddWithValue("@Tag", SqlDbType.NVarChar).Value = "Insert";
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Tag", SqlDbType.NVarChar).Value = "Update";
                    cmd.Parameters.AddWithValue("@DeviceId", SqlDbType.Int).Value = Convert.ToInt32(TxtDeviceID.Text);
                }
                cmd.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("Record has been save Succusesfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                BtnSave.Text = "Save";
                ClearContrl();
                cmd.Dispose();
                Load_grid();
            }
        }

        protected void ClearContrl()
        {
            TxtDeviceName.Text = string.Empty;
            TxtIp.Text = string.Empty;
            cmbDirection.SelectedIndex = -1;
            txtLocation.Text = string.Empty;
            TxtSerialNo.Text = string.Empty;
            cmbDeviceType.SelectedIndex = -1;
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            int i = DataGRDevices.SelectedCells[0].RowIndex;
            TxtDeviceName.Text = DataGRDevices.Rows[i].Cells[0].Value.ToString();
            TxtIp.Text = DataGRDevices.Rows[i].Cells[1].Value.ToString();
            cmbDirection.Text = DataGRDevices.Rows[i].Cells[2].Value.ToString();
            TxtSerialNo.Text = DataGRDevices.Rows[i].Cells[3].Value.ToString();
            TxtDeviceID.Text = DataGRDevices.Rows[i].Cells[4].Value.ToString();
            txtLocation.Text = DataGRDevices.Rows[i].Cells[5].Value.ToString();
            cmbDeviceType.Text = DataGRDevices.Rows[i].Cells[6].Value.ToString();
            BtnSave.Text = "Update";
        }
        private void Load_grid()
        {
            try
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
                string Query = "select * from MyDevices Where Location='" + GlobalVar.Branch + "' order by Deviceid ";
                cmd = new SqlCommand(Query, conn);
                da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                DataGRDevices.AutoGenerateColumns = false;
                DataGRDevices.DataSource = null;
                DataGRDevices.ColumnCount = 7;

                DataGRDevices.Columns[0].Name = "Device Name";
                DataGRDevices.Columns[0].HeaderText = "Device Name";
                DataGRDevices.Columns[0].DataPropertyName = "DeviceName";

                DataGRDevices.Columns[1].Name = "Ipaddress";
                DataGRDevices.Columns[1].HeaderText = "Ipaddress";
                DataGRDevices.Columns[1].DataPropertyName = "DeviceIp";
                DataGRDevices.Columns[1].Width = 120;
                DataGRDevices.Columns[2].Name = "Device Direction";
                DataGRDevices.Columns[2].HeaderText = "Device Direction";
                DataGRDevices.Columns[2].DataPropertyName = "DeviceDir";
                DataGRDevices.Columns[2].Width = 120;
                DataGRDevices.Columns[3].Name = "Serial Number";
                DataGRDevices.Columns[3].HeaderText = "Serial Number";
                DataGRDevices.Columns[3].DataPropertyName = "DeviceSrNo";

                DataGRDevices.Columns[4].Name = "DeviceID";
                DataGRDevices.Columns[4].HeaderText = "DeviceID";
                DataGRDevices.Columns[4].DataPropertyName = "DeviceID";
                DataGRDevices.Columns[4].Visible = false;

                DataGRDevices.Columns[5].Name = "Location";
                DataGRDevices.Columns[5].HeaderText = "Location";
                DataGRDevices.Columns[5].DataPropertyName = "Location";

                DataGRDevices.Columns[6].Name = "DeviceType";
                DataGRDevices.Columns[6].HeaderText = "DeviceType";
                DataGRDevices.Columns[6].DataPropertyName = "DeviceType";
                DataGRDevices.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                da.Dispose();
                cmd.Dispose();
            }
        }

        private void DataGRDevices_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void DataGRDevices_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void DataGRDevices_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                BtnEdit_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGRDevices_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dr = MessageBox.Show("Do You Want Delete this Device in Database", "Conformation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (dr == DialogResult.Yes)
                {
                    conn.Close();
                    cmd.Dispose();
                    int i = DataGRDevices.SelectedCells[0].RowIndex;
                    string Query = "delete from MyDevices where DeviceId=" + DataGRDevices.Rows[i].Cells[4].Value + "";
                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
                    conn.Open();
                    cmd = new SqlCommand(Query, conn);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Slected Device deleted Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DataGRDevices.ClearSelection();
                    Load_grid();
                    conn.Close();
                    cmd.Dispose();
                }
                else if (dr == DialogResult.No)
                {
                    return;
                }
            }
        }

        private void btnDeviceManage_Click(object sender, EventArgs e)
        {
            try
            {
                grBack.Visible = false;
                GrFront.Visible = true;
                LoadIp();
                btnDeviceManage.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnDecrypt_Click(object sender, EventArgs e)
        {
            string path = Path.Combine(Application.StartupPath, TxtSerialNo.Text + ".txt");
            string line = null;
            line = File.ReadLines(path).Skip(20).Take(1).First();
            EnValue = Convert.ToString(line);
            key = Convert.ToString(txtKey.Text);
            if (key != "" && EnValue != "")
            {
                string Decrypt = EncryptionandDecryption.DecryptStringAES(EnValue, key);
                MessageBox.Show(Decrypt);
            }
        }

        private void btnTestConnection_Click(object sender, EventArgs e)
        {
            try
            {
                string ip = cmbIp.SelectedValue.ToString();
                bIsConnected = axCZKEM1.Connect_Net(ip, Convert.ToInt32(4370));
                if (bIsConnected == true)
                {
                    MessageBox.Show("Test Connection Successfull", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    axCZKEM1.Disconnect();
                    return;
                }
                else
                {
                    MessageBox.Show("Test Connection Failed Device not Connected", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnClerLogs_Click(object sender, EventArgs e)
        {
            try
            {
                string ip = cmbIp.SelectedValue.ToString();
                bIsConnected = axCZKEM1.Connect_Net(ip, Convert.ToInt32(4370));
                int idwErrorCode = 0;
                axCZKEM1.EnableDevice(iMachineNumber, false);//disable the device
                if (axCZKEM1.ClearGLog(iMachineNumber))
                {
                    axCZKEM1.RefreshData(iMachineNumber);//the data in the device should be refreshed
                    MessageBox.Show("All att Logs have been cleared from teiminal!", "Success");
                }
                else
                {
                    axCZKEM1.GetLastError(ref idwErrorCode);
                    MessageBox.Show("Operation failed,ErrorCode=" + idwErrorCode.ToString(), "Error");
                }
                axCZKEM1.EnableDevice(iMachineNumber, true);//enable the device
                axCZKEM1.Disconnect();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                string ip = cmbIp.SelectedValue.ToString();
                bIsConnected = axCZKEM1.Connect_Net(ip, Convert.ToInt32(4370));
                if (bIsConnected == true)
                {
                    int idwErrorCode = 0;
                    int iValue = 0;
                    axCZKEM1.EnableDevice(iMachineNumber, false);//disable the device
                    if (axCZKEM1.GetDeviceStatus(iMachineNumber, 6, ref iValue)) //Here we use the function "GetDeviceStatus" to get the record's count.The parameter "Status" is 6.
                    {
                        MessageBox.Show("The count of the AttLogs in the device is " + iValue.ToString(), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        axCZKEM1.GetLastError(ref idwErrorCode);
                        MessageBox.Show("Operation failed,ErrorCode=" + idwErrorCode.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    axCZKEM1.EnableDevice(iMachineNumber, true);//enable the device
                    axCZKEM1.Disconnect();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnClearAdmin_Click(object sender, EventArgs e)
        {
            try
            {
                int idwErrorCode = 0;
                string ip = cmbIp.SelectedValue.ToString();
                bIsConnected = axCZKEM1.Connect_Net(ip, Convert.ToInt32(4370));
                if (bIsConnected == true)
                {
                    bool res = axCZKEM1.ClearAdministrators(iMachineNumber);
                    if(res == true)
                    {
                        axCZKEM1.RefreshData(iMachineNumber);
                        MessageBox.Show("Administrator privileges cleared sucessfully", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        axCZKEM1.GetLastError(ref idwErrorCode);
                        MessageBox.Show("Operation failed,ErrorCode=" + idwErrorCode.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    axCZKEM1.EnableDevice(iMachineNumber, true);
                    axCZKEM1.Disconnect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btndeviceInfo_Click(object sender, EventArgs e)
        {
            try
            {
                int idwErrorCode = 0;
                int dwStatus = 1;
                int dwValue = 0;
                string ip = cmbIp.SelectedValue.ToString();
                bIsConnected = axCZKEM1.Connect_Net(ip, Convert.ToInt32(4370));
                if (bIsConnected == true)
                {
                    var res = axCZKEM1.GetDeviceStatus(iMachineNumber, dwStatus, ref dwValue);
                    if (res == true)
                    {
                        MessageBox.Show("Administrator privileges cleared sucessfully", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        axCZKEM1.GetLastError(ref idwErrorCode);
                        MessageBox.Show("Operation failed,ErrorCode=" + idwErrorCode.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    axCZKEM1.EnableDevice(iMachineNumber, true);
                    axCZKEM1.Disconnect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnRestartDevice_Click(object sender, EventArgs e)
        {
            try
            {
                int idwErrorCode = 0;
                string ip = cmbIp.SelectedValue.ToString();
                bIsConnected = axCZKEM1.Connect_Net(ip, Convert.ToInt32(4370));
                if (bIsConnected == true)
                {
                    bool res = axCZKEM1.RestartDevice(iMachineNumber);
                    if (res == true)
                    {
                        MessageBox.Show("Device Restart Complteted successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        axCZKEM1.GetLastError(ref idwErrorCode);
                        MessageBox.Show("Operation failed,ErrorCode=" + idwErrorCode.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    axCZKEM1.EnableDevice(iMachineNumber, true);
                    axCZKEM1.Disconnect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                grBack.Visible = true;
                GrFront.Visible = false;
                btnDeviceManage.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void LoadIp()
        {
            try
            {
                DataTable data = db.getData("SP_GetDevices");
                DataTable dt = data.Select("Location ='" + GlobalVar.Branch + "'").CopyToDataTable();
                cmbIp.DataSource = null;
                cmbIp.DisplayMember = "Ip";
                cmbIp.ValueMember = "Deviceip";
                cmbIp.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
